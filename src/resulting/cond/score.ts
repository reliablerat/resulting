import {
  Bet,
  SupportedBetType,
  MatchScore,
  ScoreData,
  BetType,
} from "../types";
import {
  subtract,
  gt,
  eq,
  flow,
  over,
  property,
  spread,
  add,
  curryRight,
  curry,
  identity,
  lt,
  cond,
} from "lodash";
import {
  usePeriodScoreWithHandicap,
  useAddHandicapOnOpponent,
} from "./handicap";
import {
  isPeriodFirstHalf,
  isPeriodSecondHalf,
  isPeriodFullTime,
} from "./period";

export const useScore = ({
  score: {
    fullTime: { home: home_, away: away_ },
    firstHalf: { home: _home, away: _away },
  },
}: Bet<BetType | SupportedBetType>): MatchScore => ({
  home: home_ - _home,
  away: away_ - _away,
});
export const useFirstHalfScore = property<Bet<any>, MatchScore>(
  "score.firstHalf"
);
export const useAllScore = property<Bet<any>, ScoreData>("score");
export const useHomeScore = property<MatchScore, number>("home");
export const useAwayScore = property<MatchScore, number>("away");
export const useFullTimeScore = property<Bet<any>, MatchScore>(
  "score.fullTime"
);

export const useLiveScore = property<Bet<any>, MatchScore>("score.liveScore");
export const subtractTwoPeriodScore = (
  periodA: MatchScore,
  periodB: MatchScore
): MatchScore => ({
  home: periodA.home - periodB.home,
  away: periodA.away - periodB.away,
});

export const combineTwoPeriodScore = (
  periodA: MatchScore,
  periodB: MatchScore
): MatchScore => ({
  home: periodA.home + periodB.home,
  away: periodA.away + periodB.away,
});

export const useActualScore = (
  usePeriodScore: (data: Bet<any>) => MatchScore
) =>
  flow(
    over(
      flow(over(usePeriodScore, useLiveScore), spread(subtractTwoPeriodScore)),
      identity
    ),

    spread(useAddHandicapOnOpponent)
  );

export const useCombineHomeAwayScore = flow(
  over(useHomeScore, useAwayScore),
  spread(add)
);

export const useSubtractHomeAwayScore = flow(
  over(useHomeScore, useAwayScore),
  spread(subtract)
);

export const useSubtractAwayHomeScore = flow(
  over(useAwayScore, useHomeScore),
  spread(subtract)
);

export const useHomeSecondHalf = flow(
  over(
    flow(useFullTimeScore, useHomeScore),
    flow(useFirstHalfScore, useHomeScore)
  ),
  spread(subtract)
);

export const useAwaySecondHalf = flow(
  over(
    flow(useFullTimeScore, useAwayScore),
    flow(useFirstHalfScore, useAwayScore)
  ),
  spread(subtract)
);

export const useSecondHalfScore = flow(
  over(useHomeSecondHalf, useAwaySecondHalf),
  ([home, away]) => ({ home, away })
);

export const combineFirstHalfScore = flow(
  useFirstHalfScore,
  useCombineHomeAwayScore
);
export const combineSecondHalfScore = flow(
  over(useHomeSecondHalf, useAwaySecondHalf),
  spread(add)
);
// flow(useFirstHalfScore, useHomeScore);
// flow(useFullTimeScore, useHomeScore);
//over(useFirstHalfScore, useFullTimeScore);
export const combineFullTimeScore = flow(
  useFullTimeScore,
  useCombineHomeAwayScore
);

export const ZERO_SCORE = 0;
export const greaterThanZeroScore = curryRight(gt)(ZERO_SCORE);
export const equalToZeroScore = curryRight(eq)(ZERO_SCORE);

export const isHomeGreaterThanZero = flow(useHomeScore, greaterThanZeroScore);
export const isAwayGreaterThanZero = flow(useAwayScore, greaterThanZeroScore);
export const isHomeEqualToZero = flow(useHomeScore, equalToZeroScore);
export const isAwayEqualToZero = flow(useAwayScore, equalToZeroScore);
export const useCombineScoreOverPeriod = (
  usePeriodScore: (data: Bet<any>) => MatchScore
) => flow(usePeriodScore, useCombineHomeAwayScore);

export const compareHomeAndAwayOnActualScore = (
  usePeriod: (data: Bet<any>) => MatchScore,
  op: (value: number, othervalue: number) => boolean
) =>
  flow(useActualScore(usePeriod), over(useHomeScore, useAwayScore), spread(op));

export const compareHomeEqAwayActualScore = (
  usePeriod: (data: Bet<any>) => MatchScore
) => compareHomeAndAwayOnActualScore(usePeriod, eq);

export const compareHomeGtAwayActualScore = (
  usePeriod: (data: Bet<any>) => MatchScore
) => compareHomeAndAwayOnActualScore(usePeriod, gt);

export const compareAwayGtActualScore = (
  usePeriod: (data: Bet<any>) => MatchScore
) => compareHomeAndAwayOnActualScore(usePeriod, lt);

export const useFullTimeHomeScore = flow(useFullTimeScore, useHomeScore);
export const useFirstHalfHomeScore = flow(useFirstHalfScore, useHomeScore);
export const useFullTimeAwayScore = flow(useFullTimeScore, useAwayScore);
export const useFirstHalfAwayScore = flow(useFirstHalfScore, useAwayScore);
export const usePeriodScore = cond<Bet<any>, MatchScore>([
  [isPeriodFirstHalf, useFirstHalfScore],
  [isPeriodSecondHalf, useSecondHalfScore],
  [isPeriodFullTime, useFullTimeScore],
]);
