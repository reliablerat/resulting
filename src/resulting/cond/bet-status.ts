import { constant } from "lodash";
import { PlayerBetStatus } from "../types";

export const playerBetStatusLose = constant(PlayerBetStatus.Lose);
export const playerBetStatusHalfWin = constant(PlayerBetStatus.HalfWin);
export const playerBetStatusHalfLose = constant(PlayerBetStatus.HalfLose);
export const playerBetStatusWin = constant(PlayerBetStatus.Win);
export const playerBetStatusDraw = constant(PlayerBetStatus.Draw);
