import { CondPair } from "lodash";
import { Bet } from "../types";

export const otherwise = () => true;
export const ErrorNotSupportedBetPosition = new Error(
  "Bet position not supported"
);
export const ErrorNotSupportedBetTypes = new Error("Bet types not supported");
export const ErrorNotSupportedPeriod = new Error("Period not supported");
export const ErrorHandicapValueNotSupported = new Error(
  "Handicap value not supported"
);

export const throwErrorNotSupportedBetTypes = () => {
  throw ErrorNotSupportedBetTypes;
};
export const throwErrorNotSupportedBetPosition = () => {
  throw ErrorNotSupportedBetPosition;
};
export const throwErrorNotSupportedPeriod = () => {
  throw ErrorNotSupportedPeriod;
};

export const throwErrorNotSupportedHandicap = () => {
  throw ErrorHandicapValueNotSupported;
};

export const otherwiseError = (err: Error): CondPair<Bet<any>, void> => [
  otherwise,
  () => {
    throw err;
  },
];
