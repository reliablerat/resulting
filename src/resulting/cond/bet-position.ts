import { OddsKind, Bet, PlayerBetStatus } from "../types";
import { CondPair } from "lodash";

export const isBetPositionOn = <T>(oddsKind: OddsKind<T>) => (
  data: Bet<T>
): boolean => data.betPosition === oddsKind;

export const scenarioBetOn = <T>(
  oddsKind: OddsKind<T>,
  resultingLogic: (data: Bet<T>) => PlayerBetStatus
): CondPair<Bet<T>, PlayerBetStatus | void> => [
  isBetPositionOn<T>(oddsKind),
  resultingLogic,
];
