
export const isEven = (num: number) => num % 2 === 0;
export const isOdd = (num: number) => !isEven(num);
