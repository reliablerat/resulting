import { Bet, FavouriteType, OddsKind, BetType, MatchScore } from "../types";
import { isBetPositionOn } from "./bet-position";
import {
  cond,
  eq,
  property,
  flow,
  curry,
  over,
  spread,
  add,
  zipObject,
  identity,
  constant,
} from "lodash";
import { otherwise } from "./misc";
import { useHomeScore, useAwayScore } from "./score";

export const useHandicap = property<Bet<any>, number>("handicap");
export const isHandicapFavHome = flow<Bet<any>[], FavouriteType, boolean>(
  property("favType"),
  curry(eq)(FavouriteType.Home)
);
export const isHandicapFavAway = flow<Bet<any>[], FavouriteType, boolean>(
  property("favType"),
  curry(eq)(FavouriteType.Away)
);
export const isHandicapFav = (_favType: FavouriteType) => ({
  favType,
}: Bet<any>) => favType === _favType;

export const isBetPositionAndFav = <T>(
  favTye: FavouriteType,
  oddsKind: OddsKind<T>
) => cond([[isBetPositionOn<T>(oddsKind), isHandicapFav(favTye)]]);

export const comparisonOp = () => true;

const useFraction = (handicap: number) => handicap - Math.trunc(handicap);
const isHandicapFraction = (fractionBase: number) => ({
  handicap,
}: Bet<BetType>) => eq(useFraction(handicap + 1), fractionBase);

export const makeScoreHandicapPair = (
  usePeriodScore: (data: Bet<any>) => MatchScore,
  usePosition: (data: MatchScore) => number
) => over(flow(usePeriodScore, usePosition), useHandicap);

export const combineScoreHandicap = (
  usePeriodScore: (data: Bet<any>) => MatchScore,
  usePosition: (data: MatchScore) => number
) => flow(makeScoreHandicapPair(usePeriodScore, usePosition), spread(add));

const pairOfNumberToMatchScore = ([home, away]: number[]): MatchScore => ({
  home,
  away,
});

export const useMatchScoreHandicapFavHome = (
  usePeriodScore: (data: Bet<any>) => MatchScore
) =>
  flow(
    over(
      flow(usePeriodScore, useHomeScore),
      combineScoreHandicap(usePeriodScore, useAwayScore)
    ),
    pairOfNumberToMatchScore
  );

export const useMatchScoreHandicapFavAway = (
  usePeriodScore: (data: Bet<any>) => MatchScore
) =>
  flow(
    over(
      combineScoreHandicap(usePeriodScore, useHomeScore),
      flow(usePeriodScore, useAwayScore)
    ),
    pairOfNumberToMatchScore
  );

export const usePeriodScoreWithHandicap = (
  usePeriodScore: (data: Bet<any>) => MatchScore
) =>
  cond<Bet<any>, MatchScore>([
    [isHandicapFavHome, useMatchScoreHandicapFavHome(usePeriodScore)],
    [isHandicapFavAway, useMatchScoreHandicapFavAway(usePeriodScore)],
    [otherwise, usePeriodScore],
  ]);

export const addHandicapToAway = ({ home, away }: MatchScore) => (
  handicap: number
): MatchScore => ({
  home: home,
  away: away + handicap,
});

export const addHandicapToHome = ({ home, away }: MatchScore) => (
  handicap: number
): MatchScore => ({
  home: home + handicap,
  away: away,
});

export const useAddHandicapOnOpponent = (
  score: MatchScore,
  betData: Bet<any>
) =>
  cond<Bet<any>, MatchScore>([
    [isHandicapFavHome, flow(useHandicap, addHandicapToAway(score))],
    [isHandicapFavAway, flow(useHandicap, addHandicapToHome(score))],
    [otherwise, constant(score)],
  ])(betData);

export const HANDICAP_0_25 = 0.25;
export const HANDICAP_0_50 = 0.5;
export const HANDICAP_0_75 = 0.75;
export const HANDICAP_1_00 = 1;
export const isHandicap_0_25 = isHandicapFraction(HANDICAP_0_25);
export const isHandicap_0_50 = isHandicapFraction(HANDICAP_0_50);
export const isHandicap_0_75 = isHandicapFraction(HANDICAP_0_75);
export const isHandicap_1_00 = isHandicapFraction(0);
