import { SupportedBetType, Bet, BetType } from "../types";
import {
  property,
  curry,
  eq,
  flow,
  CondPair,
  stubTrue,
  cond,
  constant,
} from "lodash";

export const BET_CODE_PROP_NAME = "betCode";
export const useBetCode = property<Bet<any>, BetType | SupportedBetType>(
  BET_CODE_PROP_NAME
);

export const betTypeEq = (value: BetType | SupportedBetType) =>
  curry(eq)(value);

export const checkIfBetOnBetType = (betType: BetType | SupportedBetType) =>
  flow<Bet<any>[], BetType | SupportedBetType, boolean>(
    useBetCode,
    betTypeEq(betType)
  );

export const isBetOnAsianHandicap = cond([
  [checkIfBetOnBetType(BetType.FHAH), constant(true)],
  [checkIfBetOnBetType(BetType.FTAH), constant(true)],
]);

export const isBetOnOverUnder = checkIfBetOnBetType(
  BetType.FHOU || BetType.FTAH
);

export const isBetOn1x2 = checkIfBetOnBetType(BetType.FH1X2 || BetType.FT1X2);

export const isBetOnMoneyLine = checkIfBetOnBetType(SupportedBetType.MoneyLine);
export const isBetOnOddEven = checkIfBetOnBetType(SupportedBetType.OddEven);
export const isBetOnDoubleChance = checkIfBetOnBetType(
  SupportedBetType.DoubleChance
);

export const isBetOnTotalGoal = checkIfBetOnBetType(SupportedBetType.TotalGoal);
export const isBetOnHalfTimeFullTime = checkIfBetOnBetType(
  SupportedBetType.HalfTime_FullTime
);

export const isBetOnHalfTimeFullTimeOddEven = checkIfBetOnBetType(
  SupportedBetType.HalfTime_FullTimeOdd_Even
);

export const isBetOnFirstGoalLastGoal = checkIfBetOnBetType(
  SupportedBetType.FirstGoalAndLastGoal
);

export const isBetOnCorrectScore = checkIfBetOnBetType(
  SupportedBetType.CorrectScore
);

export const isBetOnBothOneNeitherScore = checkIfBetOnBetType(
  SupportedBetType.BothOneNeithertoScore
);

export const isBetOnThreeWayHandicap = checkIfBetOnBetType(
  SupportedBetType.ThreeWayHandicap
);

export const isBetOnCleansheet = checkIfBetOnBetType(
  SupportedBetType.CleanSheet
);

export const isBetOnToWinToNil = checkIfBetOnBetType(
  SupportedBetType.ToWinToNil
);

export const isBetOnToWinFromBehind = checkIfBetOnBetType(
  SupportedBetType.ToWinFromBehind
);
export const isBetOnWinningMargin = checkIfBetOnBetType(
  SupportedBetType.WinningMargin
);

export const isBetOnHighestScoringHalf = checkIfBetOnBetType(
  SupportedBetType.HighestScoringHalf
);

export const isBetOnHighestScoringHalfHome = checkIfBetOnBetType(
  SupportedBetType.HighestScoringHalfHome
);

export const isBetOnHighestScoringHalfAway = checkIfBetOnBetType(
  SupportedBetType.HighestScoringHalfAway
);

export const isBetOnHomeWinBothHalves = checkIfBetOnBetType(
  SupportedBetType.HomeWinBothHalves
);

export const isBetOnAwayWinBothHalves = checkIfBetOnBetType(
  SupportedBetType.AwayWinBothHalves
);

export const isBetOnHomeWinEitherHalves = checkIfBetOnBetType(
  SupportedBetType.HomeWinEitherHalves
);

export const isBetOnAwayWinHeigherHalves = checkIfBetOnBetType(
  SupportedBetType.AwayWinEitherHalves
);

export const isBetOnHomeScoreBothHalves = checkIfBetOnBetType(
  SupportedBetType.HomeScoreBothHalves
);

export const isBetOnAwayScoreBothHalves = checkIfBetOnBetType(
  SupportedBetType.AwayScoreBothHalves
);

export const isBetOnBothHalvesOver_1_5 = checkIfBetOnBetType(
  SupportedBetType.BothHalvesOver1_5
);

export const isBetOnBothHalvesUnder_1_5 = checkIfBetOnBetType(
  SupportedBetType.BothHalvesUnder1_5
);

export const isBetOnBothTeamToScoreIn1H2H = checkIfBetOnBetType(
  SupportedBetType.BothTeamtoScoreIn1H_2H
);

export const isBetOnHome1HToScore2HToScore = checkIfBetOnBetType(
  SupportedBetType.Home1HtoScore_2HtoScore
);

export const isBetOnAway1HToScore2HToScore = checkIfBetOnBetType(
  SupportedBetType.Away1HtoScore_2HtoScore
);

export const isBetOnBothTeamToScore = checkIfBetOnBetType(
  SupportedBetType.BothTeamtoScore
);

export const isBetOnHomeTeamOverUnder = checkIfBetOnBetType(
  SupportedBetType.HomeTeamOverUnder
);

export const isBetOnAwayTeamOverUnder = checkIfBetOnBetType(
  SupportedBetType.AwayTeamOverUnder
);
