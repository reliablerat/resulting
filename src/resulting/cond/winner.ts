import { spread, lt, gt, flow, values, subtract, cond, eq } from "lodash";
import { MatchScore, Bet, BetType, SupportedBetType } from "../types";
import {
  isPeriodFirstHalf,
  isPeriodSecondHalf,
  isPeriodFullTime,
} from "./period";
import {
  useFirstHalfScore,
  useFullTimeScore,
  compareHomeGtAwayActualScore,
  compareAwayGtActualScore,
  compareHomeEqAwayActualScore,
} from "./score";
import { otherwise, throwErrorNotSupportedPeriod } from "./misc";

const lessThan: (args: number[]) => boolean = spread<boolean>(lt);
const greaterThan: (args: number[]) => boolean = spread<boolean>(gt);
const equalThan: (args: number[]) => boolean = spread<boolean>(eq);

export const firstHalfScoreCompare = (compareFn: (n: number[]) => boolean) =>
  flow<Bet<any>[], MatchScore, number[], boolean>(
    useFirstHalfScore,
    values,
    compareFn
  );

export const fullTimeScoreCompare = (compareFn: (n: number[]) => boolean) =>
  flow<Bet<any>[], MatchScore, number[], boolean>(
    useFullTimeScore,
    values,
    compareFn
  );

export const isAwayWinFirstHalf = firstHalfScoreCompare(lessThan);
export const isAwayWinFullTime = fullTimeScoreCompare(lessThan);
export const isHomeWinFirstHalf = firstHalfScoreCompare(greaterThan);
export const isHomeWinFullTime = fullTimeScoreCompare(greaterThan);
export const isDrawFirstHalf = firstHalfScoreCompare(equalThan);
export const isDrawFulltime = fullTimeScoreCompare(equalThan);

export const isAwayWinSecondHalf = ({
  score: {
    fullTime: { home: home_, away: away_ },
    firstHalf: { home: _home, away: _away },
  },
}: Bet<BetType | SupportedBetType>) =>
  lt(subtract(home_, _home), subtract(away_, _away));

export const isHomeWinSecondHalf = ({
  score: {
    fullTime: { home: home_, away: away_ },
    firstHalf: { home: _home, away: _away },
  },
}: Bet<BetType | SupportedBetType>) =>
  gt(subtract(home_, _home), subtract(away_, _away));

export const isDrawSecondHalf = ({
  score: {
    fullTime: { home: home_, away: away_ },
    firstHalf: { home: _home, away: _away },
  },
}: Bet<BetType | SupportedBetType>) =>
  eq(subtract(home_, _home), subtract(away_, _away));

export const isAwayWin = cond([
  [isPeriodFirstHalf, isAwayWinFirstHalf],
  [isPeriodSecondHalf, isAwayWinSecondHalf],
  [isPeriodFullTime, isAwayWinFullTime],
]);

export const isHomeWin = cond([
  [isPeriodFirstHalf, isHomeWinFirstHalf],
  [isPeriodSecondHalf, isHomeWinSecondHalf],
  [isPeriodFullTime, isHomeWinFullTime],
]);

export const isDraw = cond([
  [isPeriodFirstHalf, isDrawFirstHalf],
  [isPeriodSecondHalf, isDrawSecondHalf],
  [isPeriodFullTime, isDrawFulltime],
]);

export const isHomeWinOnActualScore = cond([
  [isPeriodFirstHalf, compareHomeGtAwayActualScore(useFirstHalfScore)],
  [isPeriodFullTime, compareHomeGtAwayActualScore(useFullTimeScore)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const isDrawOnActualScore = cond([
  [isPeriodFirstHalf, compareHomeEqAwayActualScore(useFirstHalfScore)],
  [isPeriodFullTime, compareHomeEqAwayActualScore(useFullTimeScore)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const isAwayWinOnActualScore = cond([
  [isPeriodFirstHalf, compareAwayGtActualScore(useFirstHalfScore)],
  [isPeriodFullTime, compareAwayGtActualScore(useFullTimeScore)],
  [otherwise, throwErrorNotSupportedPeriod],
]);
