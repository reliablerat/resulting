import { property, eq, curry, flow } from "lodash";
import { Bet, Period } from "../types";

export const usePeriod = property<Bet<any>, Period>("period");
export const periodEq = (value: Period) =>
  curry<Period, Period, boolean>(eq)(value);
export const compareWithPeriod = (period: Period) =>
  flow<Bet<any>[], Period, boolean>(usePeriod, periodEq(period));
export const isPeriodFirstHalf = compareWithPeriod(Period.firstHalf);
export const isPeriodSecondHalf = compareWithPeriod(Period.secondHalf);
export const isPeriodFullTime = compareWithPeriod(Period.fullTime);
