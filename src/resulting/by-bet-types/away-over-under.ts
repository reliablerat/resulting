import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curryRight,
  eq,
  overEvery,
  overSome,
  subtract,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayTeamToScoreIn1H2HKind,
  MatchScore,
  OddsBothTeamToScoreKind,
  OddsOverUnderKind,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
  playerBetStatusHalfWin,
  playerBetStatusHalfLose,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFirstHalf, isPeriodSecondHalf } from "../cond/period";
import {
  useHomeScore,
  useAwayScore,
  useFirstHalfScore,
  useSecondHalfScore,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";
import { useHandicap } from "../cond/handicap";

const awayHandicapCompare = (
  useOp: (value: number, otherValue: number) => boolean
) => flow(over(useFirstHalfAwayScore, useHandicap), spread(useOp));

const awayHandicapSubtractCompare = (
  useOp: (value: number, otherValue: number) => boolean,
  handicapComparisonValue: number
) =>
  flow(
    over(useFirstHalfAwayScore, useHandicap),
    spread(subtract),
    curryRight(useOp)(handicapComparisonValue)
  );

const awayHandicapAndLogic = (
  leftOp: (value: number, otherValue: number) => boolean,
  rightOp: (value: number, otherValue: number) => boolean,
  handicapComparisonValue: number
) =>
  overEvery(
    awayHandicapCompare(leftOp),
    awayHandicapSubtractCompare(rightOp, handicapComparisonValue)
  );

const awayTeamOverUnderOnOver = cond([
  [awayHandicapAndLogic(gt, eq, 0.25), playerBetStatusHalfWin],
  [awayHandicapAndLogic(gt, gt, 0.25), playerBetStatusWin],
  [awayHandicapCompare(eq), playerBetStatusDraw],
  [awayHandicapAndLogic(lt, eq, -0.25), playerBetStatusHalfLose],
  [awayHandicapAndLogic(lt, lt, -0.25), playerBetStatusLose],
]);

const awayTeamOverUnderOnUnder = cond([
  [awayHandicapAndLogic(lt, eq, -0.25), playerBetStatusHalfWin],
  [awayHandicapAndLogic(lt, lt, -0.25), playerBetStatusWin],
  [awayHandicapCompare(eq), playerBetStatusDraw],
  [awayHandicapAndLogic(gt, eq, 0.25), playerBetStatusHalfLose],
  [awayHandicapAndLogic(gt, gt, 0.25), playerBetStatusLose],
]);

const awayTeamOverUnderResulting = (
  useLogic: (data: Bet<any>) => PlayerBetStatus
) =>
  cond([
    [isPeriodFirstHalf, useLogic],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingAwayTeamOverUnder = cond([
  scenarioBetOn<SupportedBetType.AwayTeamOverUnder>(
    OddsOverUnderKind.Over,
    awayTeamOverUnderResulting(awayTeamOverUnderOnOver)
  ),
  scenarioBetOn<SupportedBetType.AwayTeamOverUnder>(
    OddsOverUnderKind.Under,
    awayTeamOverUnderResulting(awayTeamOverUnderOnUnder)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
