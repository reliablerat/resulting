import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curry,
  curryRight,
  stubTrue,
  eq,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHomeScoreBothHalvesKind,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const homeFirstHalfCompareToZero = (
  useOperation: (value: number, oterhValue: number) => boolean
) => flow(useFirstHalfHomeScore, curryRight(useOperation)(0));
const homeSecondHalfCompareToZero = (
  useOperation: (value: number, oterhValue: number) => boolean
) => flow(useHomeSecondHalf, curryRight(useOperation)(0));

const homeScoreBothHalvesYesCondition = cond([
  [homeFirstHalfCompareToZero(gt), homeSecondHalfCompareToZero(gt)],
  [otherwise, stubFalse],
]);

const homeScoreBothHalvesNoCondition = cond([
  [homeFirstHalfCompareToZero(eq), stubTrue],
  [homeSecondHalfCompareToZero(eq), stubTrue],
  [otherwise, stubFalse],
]);

const resultingHomeScoreBothHalvesOnBetStatusLogic = (
  useLogic: (Target: Bet<any>) => boolean
) =>
  cond([
    [useLogic, playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHomeScoreBothHalvesOnYes = cond([
  [
    isPeriodFullTime,
    resultingHomeScoreBothHalvesOnBetStatusLogic(
      homeScoreBothHalvesYesCondition
    ),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHomeScoreBothHalvesOnNo = cond([
  [
    isPeriodFullTime,
    resultingHomeScoreBothHalvesOnBetStatusLogic(
      homeScoreBothHalvesNoCondition
    ),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingHomeScoreBothHalves = cond([
  scenarioBetOn<SupportedBetType.HomeScoreBothHalves>(
    OddsHomeScoreBothHalvesKind.Yes,
    resultingHomeScoreBothHalvesOnYes
  ),
  scenarioBetOn<SupportedBetType.HomeScoreBothHalves>(
    OddsHomeScoreBothHalvesKind.No,
    resultingHomeScoreBothHalvesOnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
