import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curry,
  curryRight,
  stubTrue,
  eq,
  add,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsBothHalvesUnder1_5Kind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
  useFirstHalfScore,
  useCombineHomeAwayScore,
} from "../cond/score";

const combineScoreFirstHalfCompareTo1_5 = (
  useOperation: (value: number, oterhValue: number) => boolean
) =>
  flow(
    useFirstHalfScore,
    useCombineHomeAwayScore,
    curryRight(useOperation)(1.5)
  );
const combineScoreSecondHalfCompareTo1_5 = (
  useOperation: (value: number, oterhValue: number) => boolean
) =>
  flow(
    over(useHomeSecondHalf, useAwaySecondHalf),
    spread(add),
    curryRight(useOperation)(1.5)
  );

const BothHalvesUnder1_5YesCondition = cond([
  [
    combineScoreFirstHalfCompareTo1_5(lt),
    combineScoreSecondHalfCompareTo1_5(lt),
  ],
  [otherwise, stubFalse],
]);

const BothHalvesUnder1_5NoCondition = cond([
  [combineScoreFirstHalfCompareTo1_5(gt), stubTrue],
  [combineScoreSecondHalfCompareTo1_5(gt), stubTrue],
  [otherwise, stubFalse],
]);

const resultingBothHalvesUnder1_5OnBetStatusLogic = (
  useLogic: (Target: Bet<any>) => boolean
) =>
  cond([
    [useLogic, playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingBothHalvesUnder1_5OnYes = cond([
  [
    isPeriodFullTime,
    resultingBothHalvesUnder1_5OnBetStatusLogic(
      BothHalvesUnder1_5YesCondition
    ),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingBothHalvesUnder1_5OnNo = cond([
  [
    isPeriodFullTime,
    resultingBothHalvesUnder1_5OnBetStatusLogic(
      BothHalvesUnder1_5NoCondition
    ),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingBothHalvesUnder1_5 = cond([
  scenarioBetOn<SupportedBetType.BothHalvesUnder1_5>(
    OddsBothHalvesUnder1_5Kind.Yes,
    resultingBothHalvesUnder1_5OnYes
  ),
  scenarioBetOn<SupportedBetType.BothHalvesUnder1_5>(
    OddsBothHalvesUnder1_5Kind.No,
    resultingBothHalvesUnder1_5OnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
