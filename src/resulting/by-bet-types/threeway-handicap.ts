import {
  cond,
  add,
  constant,
  subtract,
  flatten,
  overEvery,
  over,
  flow,
  spread,
  curryRight,
  gt,
  eq,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsThreeWayHandicapKind,
  MatchScore,
  MatchScoreAndHandicap,
  FavouriteType,
  OddsKind,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn, isBetPositionOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  isHandicapFavHome,
  isHandicapFavAway,
  isHandicapFav,
  isBetPositionAndFav,
  useHandicap,
} from "../cond/handicap";
import { useFullTimeHomeScore, useFullTimeAwayScore } from "../cond/score";

type LogicParams = {
  useHandicapFav: (data: Bet<any>) => boolean;
  useLeftScore: (data: Bet<any>) => number;
  useRightScore: (data: Bet<any>) => number;
  useHandicapOp: (value: number, otherValue: number) => number;
  useHandicapComparison: (value: number, otherValue: number) => boolean;
};

// const b = (handicap: number) => (score: number) => op(handicap, number);

const threewayHandicapLogic = ({
  useHandicapFav,
  useLeftScore,
  useRightScore,
  useHandicapOp,
  useHandicapComparison,
}: LogicParams) =>
  overEvery(
    useHandicapFav,
    flow(
      over(
        flow(over(useLeftScore, useHandicap), spread(useHandicapOp)),
        useRightScore
      ),
      spread(useHandicapComparison)
    )
  );

const isFavHomeLogicParams = {
  useHandicapFav: isHandicapFavHome,
  useLeftScore: useFullTimeHomeScore,
  useRightScore: useFullTimeAwayScore,
  useHandicapOp: subtract,
  useHandicapComparison: gt,
};

const isFavHomeLogicOnDrawParams = {
  ...isFavHomeLogicParams,
  useLeftScore: useFullTimeAwayScore,
  useRightScore: useFullTimeHomeScore,
  useHandicapOp: add,
  useHandicapComparison: eq,
};

const isFavHomeLogicOnAwayParams = {
  ...isFavHomeLogicOnDrawParams,
  useHandicapComparison: gt,
};

const betOnHomeLogic = cond([
  [threewayHandicapLogic(isFavHomeLogicParams), playerBetStatusWin],
  [
    threewayHandicapLogic({
      ...isFavHomeLogicParams,
      useHandicapFav: isHandicapFavAway,
      useHandicapOp: add,
    }),
    playerBetStatusWin,
  ],
  [otherwise, playerBetStatusLose],
]);

const betOnDrawLogic = cond([
  [threewayHandicapLogic(isFavHomeLogicOnDrawParams), playerBetStatusWin],
  [
    threewayHandicapLogic({
      ...isFavHomeLogicOnDrawParams,
      useHandicapFav: isHandicapFavAway,
      useHandicapOp: subtract,
    }),
    playerBetStatusWin,
  ],
  [otherwise, playerBetStatusLose],
]);

const betOnLoseLogic = cond([
  [threewayHandicapLogic(isFavHomeLogicOnAwayParams), playerBetStatusWin],
  [
    threewayHandicapLogic({
      ...isFavHomeLogicOnAwayParams,
      useHandicapFav: isHandicapFavAway,
      useHandicapOp: subtract,
    }),
    playerBetStatusWin,
  ],
  [otherwise, playerBetStatusLose],
]);

const threeWayHandicapResultingOnPeriod = (
  useLogic: (data: Bet<any>) => PlayerBetStatus
) =>
  cond([
    [isPeriodFullTime, useLogic],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingThreewayHandicap = cond([
  scenarioBetOn<SupportedBetType.ThreeWayHandicap>(
    OddsThreeWayHandicapKind.Home,
    threeWayHandicapResultingOnPeriod(betOnHomeLogic)
  ),
  scenarioBetOn<SupportedBetType.ThreeWayHandicap>(
    OddsThreeWayHandicapKind.Draw,
    threeWayHandicapResultingOnPeriod(betOnDrawLogic)
  ),
  scenarioBetOn<SupportedBetType.ThreeWayHandicap>(
    OddsThreeWayHandicapKind.Away,
    threeWayHandicapResultingOnPeriod(betOnLoseLogic)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
