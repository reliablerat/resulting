import {
  cond,
  spread,
  gt,
  over,
  flow,
  lt,
  eq,
  subtract,
  constant,
} from "lodash";
import {
  Bet,
  BetType,
  OddsOverUnderKind,
  MatchScore,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
  playerBetStatusHalfWin,
  playerBetStatusHalfLose,
} from "../cond/bet-status";

import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedHandicap,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import {
  isHandicap_0_25,
  isHandicap_0_50,
  isHandicap_1_00,
  useHandicap,
  isHandicap_0_75,
} from "../cond/handicap";
import {
  useCombineHomeAwayScore,
  useFirstHalfScore,
  useFullTimeScore,
} from "../cond/score";
import { isPeriodFirstHalf, isPeriodFullTime } from "../cond/period";

export type HandicapComparisonOp = (value: number, other: number) => boolean;
export type WinOp = {
  firsthalf: (value: number, other: number) => boolean;
  fulltime: (value: number, other: number) => boolean;
};

export type ResultByHandicapParams = {
  handicapCompare: number[];
  handicapOp: HandicapComparisonOp;
  firstComparisonBetStatus: () => PlayerBetStatus;
};

const isCombineScoreComparison = ({
  usePeriodScore,
  op,
}: {
  usePeriodScore: (data: Bet<any>) => MatchScore;
  op: (value: number, other: number) => boolean;
}) =>
  flow(over(flow(usePeriodScore, useCombineHomeAwayScore), useHandicap), spread(op));

const useOverUnderResultingByOp = ({
  usePeriodScore,
  winOp,
}: {
  usePeriodScore: (data: Bet<any>) => MatchScore;
  winOp: (value: number, other: number) => boolean;
}) =>
  cond([
    [
      isCombineScoreComparison({ usePeriodScore, op: winOp }),
      playerBetStatusWin,
    ],
    [isCombineScoreComparison({ usePeriodScore, op: eq }), playerBetStatusDraw],
    [otherwise, playerBetStatusLose],
  ]);

const resultHandicapByPeriod = (
  winOp: (value: number, other: number) => boolean
) =>
  cond([
    [
      isPeriodFirstHalf,
      useOverUnderResultingByOp({ usePeriodScore: useFirstHalfScore, winOp }),
    ],
    [
      isPeriodFullTime,
      useOverUnderResultingByOp({ usePeriodScore: useFullTimeScore, winOp }),
    ],
  ]);

const isHandicapCompare = ({
  handicapCompare,
  usePeriodScore,
  op,
}: {
  handicapCompare: number;
  usePeriodScore: (data: Bet<any>) => MatchScore;
  op: (value: number, other: number) => boolean;
}) =>
  flow(
    over(
      flow(
        over(flow(usePeriodScore, useCombineHomeAwayScore), useHandicap),
        spread(subtract)
      ),
      constant(handicapCompare)
    ),
    spread(op)
  );

const useOverUnderResultingByOpWithComparison = ({
  handicapCompare: [firstHandicapCompare, secondHandicapCompare],
  usePeriodScore,
  handicapOp,
  firstComparisonBetStatus,
}: {
  usePeriodScore: (data: Bet<any>) => MatchScore;
  handicapCompare: number[];
  handicapOp: (value: number, other: number) => boolean;
  firstComparisonBetStatus: () => PlayerBetStatus;
}) =>
  cond([
    [
      isHandicapCompare({
        handicapCompare: firstHandicapCompare,
        usePeriodScore,
        op: eq,
      }),
      firstComparisonBetStatus,
    ],
    [
      isHandicapCompare({
        handicapCompare: secondHandicapCompare,
        usePeriodScore,
        op: handicapOp,
      }),
      playerBetStatusWin,
    ],
    [otherwise, playerBetStatusLose],
  ]);

const resultHandicapByPeriodWithComparison = ({
  handicapCompare,
  handicapOp,
  firstComparisonBetStatus,
}: ResultByHandicapParams) =>
  cond([
    [
      isPeriodFirstHalf,
      useOverUnderResultingByOpWithComparison({
        handicapCompare: handicapCompare,
        usePeriodScore: useFirstHalfScore,
        handicapOp,
        firstComparisonBetStatus,
      }),
    ],
    [
      isPeriodFullTime,
      useOverUnderResultingByOpWithComparison({
        handicapCompare: handicapCompare,
        usePeriodScore: useFullTimeScore,
        handicapOp,
        firstComparisonBetStatus,
      }),
    ],
  ]);

const overOnHandicap_0_25Data: ResultByHandicapParams = {
  handicapCompare: [-0.25, 0.25],
  handicapOp: gt,
  firstComparisonBetStatus: playerBetStatusHalfLose,
};

const underOnHandicap_25Data: ResultByHandicapParams = {
  handicapCompare: [-0.25, -0.25],
  handicapOp: lt,
  firstComparisonBetStatus: playerBetStatusHalfWin,
};

const overOnHandicap_0_75Data: ResultByHandicapParams = {
  handicapCompare: [0.25, 0.25],
  handicapOp: gt,
  firstComparisonBetStatus: playerBetStatusHalfWin,
};

const underOnHandicap_0_75Data: ResultByHandicapParams = {
  handicapCompare: [0.25, 0.25],
  handicapOp: lt,
  firstComparisonBetStatus: playerBetStatusHalfLose,
};

const resultingOverUnderOnOver = cond([
  [
    isHandicap_0_25,
    resultHandicapByPeriodWithComparison(overOnHandicap_0_25Data),
  ],
  [isHandicap_0_50, resultHandicapByPeriod(gt)],
  [
    isHandicap_0_75,
    resultHandicapByPeriodWithComparison(overOnHandicap_0_75Data),
  ],
  [isHandicap_1_00, resultHandicapByPeriod(gt)],
  [otherwise, throwErrorNotSupportedHandicap],
]);

const resultingOverUnderOnUnder = cond([
  [
    isHandicap_0_25,
    resultHandicapByPeriodWithComparison(underOnHandicap_25Data),
  ],
  [isHandicap_0_50, resultHandicapByPeriod(lt)],
  [
    isHandicap_0_75,
    resultHandicapByPeriodWithComparison(underOnHandicap_0_75Data),
  ],
  [isHandicap_1_00, resultHandicapByPeriod(lt)],
  [otherwise, throwErrorNotSupportedHandicap],
]);

export const resultingOverunder = cond([
  scenarioBetOn<BetType.FHOU | BetType.FTOU>(
    OddsOverUnderKind.Over,
    resultingOverUnderOnOver
  ),
  scenarioBetOn<BetType.FHOU | BetType.FTOU>(
    OddsOverUnderKind.Under,
    resultingOverUnderOnUnder
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
