import * as _ from "lodash";
import { SupportedBetType, Bet, OddsDoubleChance } from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin, isDraw } from "../cond/winner";
import { otherwise, throwErrorNotSupportedBetPosition } from "../cond/misc";
import { isBetPositionOn } from "../cond/bet-position";


const resultingDoubleChanceHomeOrDraw = _.cond([
  [isHomeWin, playerBetStatusWin],
  [isDraw, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const resultingDoubleChanceHomeOrAway = _.cond([
  [isHomeWin, playerBetStatusWin],
  [isAwayWin, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const resultingDoubleChanceAwayOrDraw = _.cond([
  [isAwayWin, playerBetStatusWin],
  [isDraw, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

export const resultingDoubleChance = _.cond([
  [
    isBetPositionOn<SupportedBetType.DoubleChance>(OddsDoubleChance.HomeDraw),
    resultingDoubleChanceHomeOrDraw,
  ],
  [
    isBetPositionOn<SupportedBetType.DoubleChance>(OddsDoubleChance.HomeAway),
    resultingDoubleChanceHomeOrAway,
  ],
  [
    isBetPositionOn<SupportedBetType.DoubleChance>(OddsDoubleChance.AwayDraw),
    resultingDoubleChanceAwayOrDraw,
  ],
  [otherwise, throwErrorNotSupportedBetPosition],
]);
