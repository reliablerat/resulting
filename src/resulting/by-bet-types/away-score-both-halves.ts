import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curry,
  curryRight,
  stubTrue,
  eq,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayScoreBothHalvesKind,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const homeFirstHalfCompareToZero = (
  useOperation: (value: number, oterhValue: number) => boolean
) => flow(useFirstHalfAwayScore, curryRight(useOperation)(0));
const homeSecondHalfCompareToZero = (
  useOperation: (value: number, oterhValue: number) => boolean
) => flow(useAwaySecondHalf, curryRight(useOperation)(0));

const AwayScoreBothHalvesYesCondition = cond([
  [homeFirstHalfCompareToZero(gt), homeSecondHalfCompareToZero(gt)],
  [otherwise, stubFalse],
]);

const AwayScoreBothHalvesNoCondition = cond([
  [homeFirstHalfCompareToZero(eq), stubTrue],
  [homeSecondHalfCompareToZero(eq), stubTrue],
  [otherwise, stubFalse],
]);

const resultingAwayScoreBothHalvesOnBetStatusLogic = (
  useLogic: (Target: Bet<any>) => boolean
) =>
  cond([
    [useLogic, playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingAwayScoreBothHalvesOnYes = cond([
  [
    isPeriodFullTime,
    resultingAwayScoreBothHalvesOnBetStatusLogic(
      AwayScoreBothHalvesYesCondition
    ),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayScoreBothHalvesOnNo = cond([
  [
    isPeriodFullTime,
    resultingAwayScoreBothHalvesOnBetStatusLogic(
      AwayScoreBothHalvesNoCondition
    ),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingAwayScoreBothHalves = cond([
  scenarioBetOn<SupportedBetType.AwayScoreBothHalves>(
    OddsAwayScoreBothHalvesKind.Yes,
    resultingAwayScoreBothHalvesOnYes
  ),
  scenarioBetOn<SupportedBetType.AwayScoreBothHalves>(
    OddsAwayScoreBothHalvesKind.No,
    resultingAwayScoreBothHalvesOnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
