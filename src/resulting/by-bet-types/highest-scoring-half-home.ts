import { cond, flow, over, spread, subtract, lt, gt, eq } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHighestScoringHalfKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useFullTimeScore,
  useCombineHomeAwayScore,
  combineFullTimeScore,
  combineFirstHalfScore,
  useHomeScore,
  useFirstHalfScore,
  useFullTimeHomeScore,
  useFirstHalfHomeScore,
} from "../cond/score";

const highestScoringHalfHomeLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(
      flow(over(useFullTimeHomeScore, useFirstHalfHomeScore), spread(subtract)),
      useFirstHalfHomeScore
    ),
    spread(useOperation)
  );

const highestScoringHalfHomeCond = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [highestScoringHalfHomeLogic(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHighestScoringHalfHomeFirstHalf = cond([
  [isPeriodFullTime, highestScoringHalfHomeCond(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHighestScoringHalfHomeSecondHalf = cond([
  [isPeriodFullTime, highestScoringHalfHomeCond(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHighestScoringHalfHomeTied = cond([
  [isPeriodFullTime, highestScoringHalfHomeCond(eq)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingHighestScoringHalfHome = cond([
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.FirstHalf,
    resultingHighestScoringHalfHomeFirstHalf
  ),
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.SecondHalf,
    resultingHighestScoringHalfHomeSecondHalf
  ),
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.Tie,
    resultingHighestScoringHalfHomeTied
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
