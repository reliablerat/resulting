import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curryRight,
  eq,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHomeTeamToScoreIn1H2HKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const homeFirstHalfCompareWithZero = (
  useComparisonOp: (value: number, othervalue: number) => boolean
) => flow(useFirstHalfHomeScore, curryRight(useComparisonOp)(0));

const homeSecondHalfCompareWithZero = (
  useComparisonOp: (value: number, othervalue: number) => boolean
) => flow(useHomeSecondHalf, curryRight(useComparisonOp)(0));

const homeTeamToScoreIn1H2HWinCondition = (
  useComparisonLeft: (value: number, othervalue: number) => boolean,
  useComparisonRight: (value: number, othervalue: number) => boolean
) =>
  cond([
    [
      homeFirstHalfCompareWithZero(useComparisonLeft),
      homeSecondHalfCompareWithZero(useComparisonRight),
    ],
    [otherwise, stubFalse],
  ]);

const homeTeamToScoreIn1H2HResultingBetStatus = (
  useComparisonLeft: (value: number, othervalue: number) => boolean,
  useComparisonRight: (value: number, othervalue: number) => boolean
) =>
  cond([
    [
      homeTeamToScoreIn1H2HWinCondition(useComparisonLeft, useComparisonRight),
      playerBetStatusWin,
    ],
    [otherwise, playerBetStatusLose],
  ]);

const homeTeamToScoreIn1H2HResulting = (
  useComparisonLeft: (value: number, othervalue: number) => boolean,
  useComparisonRight: (value: number, othervalue: number) => boolean
) =>
  cond([
    [
      isPeriodFullTime,
      homeTeamToScoreIn1H2HResultingBetStatus(
        useComparisonLeft,
        useComparisonRight
      ),
    ],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingHome1HtoScore_2HtoScore = cond([
  scenarioBetOn<SupportedBetType.Home1HtoScore_2HtoScore>(
    OddsHomeTeamToScoreIn1H2HKind.YesYes,
    homeTeamToScoreIn1H2HResulting(gt, gt)
  ),
  scenarioBetOn<SupportedBetType.Home1HtoScore_2HtoScore>(
    OddsHomeTeamToScoreIn1H2HKind.YesNo,
    homeTeamToScoreIn1H2HResulting(gt, eq)
  ),
  scenarioBetOn<SupportedBetType.Home1HtoScore_2HtoScore>(
    OddsHomeTeamToScoreIn1H2HKind.NoYes,
    homeTeamToScoreIn1H2HResulting(eq, gt)
  ),
  scenarioBetOn<SupportedBetType.Home1HtoScore_2HtoScore>(
    OddsHomeTeamToScoreIn1H2HKind.NoNo,
    homeTeamToScoreIn1H2HResulting(eq, eq)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
