import { cond } from "lodash";
import { SupportedBetType, Bet, OddsHalfTimeFullTimeKind } from "../types";
import { playerBetStatusWin, playerBetStatusLose } from "../cond/bet-status";
import {
  isHomeWinFirstHalf,
  isHomeWinFullTime,
  isDrawFulltime,
  isAwayWinFullTime,
  isDrawFirstHalf,
  isAwayWinFirstHalf,
} from "../cond/winner";
import {
  otherwise,
  throwErrorNotSupportedBetPosition,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { isPeriodFullTime } from "../cond/period";
import { isBetPositionOn } from "../cond/bet-position";

const expectHTFTScore = (
  firstHalfScoreCondition: (data: Bet<any>) => boolean,
  fulltimeScoreCondition: (data: Bet<any>) => boolean
) =>
  cond([
    [
      cond([[firstHalfScoreCondition, fulltimeScoreCondition]]),
      playerBetStatusWin,
    ],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHTFT = (
  firstHalfScoreCondition: (data: Bet<any>) => boolean,
  fulltimeScoreCondition: (data: Bet<any>) => boolean
) =>
  cond([
    [
      isPeriodFullTime,
      expectHTFTScore(firstHalfScoreCondition, fulltimeScoreCondition),
    ],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

const resultingHTFTHomeHome = resultingHTFT(
  isHomeWinFirstHalf,
  isHomeWinFullTime
);
const resultingHTFTHomeDraw = resultingHTFT(isHomeWinFirstHalf, isDrawFulltime);
const resultingHTFTHomeAway = resultingHTFT(
  isHomeWinFirstHalf,
  isAwayWinFullTime
);
const resultingHTFTDrawHome = resultingHTFT(isDrawFirstHalf, isHomeWinFullTime);
const resultingHTFTDrawDraw = resultingHTFT(isDrawFirstHalf, isDrawFulltime);
const resultingHTFTDrawAway = resultingHTFT(isDrawFirstHalf, isAwayWinFullTime);
const resultingHTFTAwayHome = resultingHTFT(
  isAwayWinFirstHalf,
  isHomeWinFullTime
);
const resultingHTFTAwayDraw = resultingHTFT(isAwayWinFirstHalf, isDrawFulltime);
const resultingHTFTAwayAway = resultingHTFT(
  isAwayWinFirstHalf,
  isAwayWinFullTime
);

export const resultingHalfTimeFullTime = cond([
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.HomeHome
    ),
    resultingHTFTHomeHome,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.HomeDraw
    ),
    resultingHTFTHomeDraw,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.HomeAway
    ),
    resultingHTFTHomeAway,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.DrawHome
    ),
    resultingHTFTDrawHome,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.DrawDraw
    ),
    resultingHTFTDrawDraw,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.DrawAway
    ),
    resultingHTFTDrawAway,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.AwayHome
    ),
    resultingHTFTAwayHome,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.AwayDraw
    ),
    resultingHTFTAwayDraw,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTime>(
      OddsHalfTimeFullTimeKind.AwayAway
    ),
    resultingHTFTAwayAway,
  ],
  [otherwise, throwErrorNotSupportedBetPosition],
]);
