import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curry,
  curryRight,
  stubTrue,
  eq,
  add,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsBothHalvesOver1_5Kind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
  useFirstHalfScore,
  useCombineHomeAwayScore,
} from "../cond/score";

const combineScoreFirstHalfCompareTo1_5 = (
  useOperation: (value: number, oterhValue: number) => boolean
) =>
  flow(
    useFirstHalfScore,
    useCombineHomeAwayScore,
    curryRight(useOperation)(1.5)
  );
const combineScoreSecondHalfCompareTo1_5 = (
  useOperation: (value: number, oterhValue: number) => boolean
) =>
  flow(
    over(useHomeSecondHalf, useAwaySecondHalf),
    spread(add),
    curryRight(useOperation)(1.5)
  );

const BothHalvesOver1_5YesCondition = cond([
  [
    combineScoreFirstHalfCompareTo1_5(gt),
    combineScoreSecondHalfCompareTo1_5(gt),
  ],
  [otherwise, stubFalse],
]);

const BothHalvesOver1_5NoCondition = cond([
  [combineScoreFirstHalfCompareTo1_5(lt), stubTrue],
  [combineScoreSecondHalfCompareTo1_5(lt), stubTrue],
  [otherwise, stubFalse],
]);

const resultingBothHalvesOver1_5OnBetStatusLogic = (
  useLogic: (Target: Bet<any>) => boolean
) =>
  cond([
    [useLogic, playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingBothHalvesOver1_5OnYes = cond([
  [
    isPeriodFullTime,
    resultingBothHalvesOver1_5OnBetStatusLogic(BothHalvesOver1_5YesCondition),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingBothHalvesOver1_5OnNo = cond([
  [
    isPeriodFullTime,
    resultingBothHalvesOver1_5OnBetStatusLogic(BothHalvesOver1_5NoCondition),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingBothHalvesOver1_5 = cond([
  scenarioBetOn<SupportedBetType.BothHalvesOver1_5>(
    OddsBothHalvesOver1_5Kind.Yes,
    resultingBothHalvesOver1_5OnYes
  ),
  scenarioBetOn<SupportedBetType.BothHalvesOver1_5>(
    OddsBothHalvesOver1_5Kind.No,
    resultingBothHalvesOver1_5OnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
