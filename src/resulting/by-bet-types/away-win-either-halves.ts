import { cond, over, flow, spread, gt, stubFalse, lt, stubTrue } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayWinEitherHalvesKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const secondHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) => flow(over(useHomeSecondHalf, useAwaySecondHalf), spread(useOperation));

const firstHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(useFirstHalfHomeScore, useFirstHalfAwayScore),
    spread(useOperation)
  );

const AwayWinEitherHalvesResulting = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [firstHalfHomeAwayComparison(useOperation), stubTrue],
    [secondHalfHomeAwayComparison(useOperation), stubTrue],
    [otherwise, stubFalse],
  ]);

const resultingAwayWinEitherHalvesLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [AwayWinEitherHalvesResulting(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingAwayWinEitherHalvesOnYes = cond([
  [isPeriodFullTime, resultingAwayWinEitherHalvesLogic(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayWinEitherHalvesOnNo = cond([
  [isPeriodFullTime, resultingAwayWinEitherHalvesLogic(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingAwayWinEitherHalves = cond([
  scenarioBetOn<SupportedBetType.AwayWinEitherHalves>(
    OddsAwayWinEitherHalvesKind.Yes,
    resultingAwayWinEitherHalvesOnYes
  ),
  scenarioBetOn<SupportedBetType.AwayWinEitherHalves>(
    OddsAwayWinEitherHalvesKind.No,
    resultingAwayWinEitherHalvesOnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
