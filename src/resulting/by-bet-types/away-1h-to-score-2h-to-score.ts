import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curryRight,
  eq,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayTeamToScoreIn1H2HKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const awayFirstHalfCompareWithZero = (
  useComparisonOp: (value: number, othervalue: number) => boolean
) => flow(useFirstHalfAwayScore, curryRight(useComparisonOp)(0));

const awaySecondHalfCompareWithZero = (
  useComparisonOp: (value: number, othervalue: number) => boolean
) => flow(useAwaySecondHalf, curryRight(useComparisonOp)(0));

const awayTeamToScoreIn1H2HWinCondition = (
  useComparisonLeft: (value: number, othervalue: number) => boolean,
  useComparisonRight: (value: number, othervalue: number) => boolean
) =>
  cond([
    [
      awayFirstHalfCompareWithZero(useComparisonLeft),
      awaySecondHalfCompareWithZero(useComparisonRight),
    ],
    [otherwise, stubFalse],
  ]);

const awayTeamToScoreIn1H2HResultingBetStatus = (
  useComparisonLeft: (value: number, othervalue: number) => boolean,
  useComparisonRight: (value: number, othervalue: number) => boolean
) =>
  cond([
    [
      awayTeamToScoreIn1H2HWinCondition(useComparisonLeft, useComparisonRight),
      playerBetStatusWin,
    ],
    [otherwise, playerBetStatusLose],
  ]);

const awayTeamToScoreIn1H2HResulting = (
  useComparisonLeft: (value: number, othervalue: number) => boolean,
  useComparisonRight: (value: number, othervalue: number) => boolean
) =>
  cond([
    [
      isPeriodFullTime,
      awayTeamToScoreIn1H2HResultingBetStatus(
        useComparisonLeft,
        useComparisonRight
      ),
    ],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingAway1HtoScore_2HtoScore = cond([
  scenarioBetOn<SupportedBetType.Away1HtoScore_2HtoScore>(
    OddsAwayTeamToScoreIn1H2HKind.YesYes,
    awayTeamToScoreIn1H2HResulting(gt, gt)
  ),
  scenarioBetOn<SupportedBetType.Away1HtoScore_2HtoScore>(
    OddsAwayTeamToScoreIn1H2HKind.YesNo,
    awayTeamToScoreIn1H2HResulting(gt, eq)
  ),
  scenarioBetOn<SupportedBetType.Away1HtoScore_2HtoScore>(
    OddsAwayTeamToScoreIn1H2HKind.NoYes,
    awayTeamToScoreIn1H2HResulting(eq, gt)
  ),
  scenarioBetOn<SupportedBetType.Away1HtoScore_2HtoScore>(
    OddsAwayTeamToScoreIn1H2HKind.NoNo,
    awayTeamToScoreIn1H2HResulting(eq, eq)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
