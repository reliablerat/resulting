import { cond } from "lodash";
import { SupportedBetType, Bet, OddsMoneyLineKind } from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";

const resultingMoneyLineHome = cond([
  [isHomeWin, playerBetStatusWin],
  [isAwayWin, playerBetStatusLose],
  [otherwise, playerBetStatusDraw],
]);

const resultingMoneyLineAway = cond([
  [isAwayWin, playerBetStatusWin],
  [isHomeWin, playerBetStatusLose],
  [otherwise, playerBetStatusDraw],
]);

export const resultingMoneyLine = cond([
  scenarioBetOn<SupportedBetType.MoneyLine>(
    OddsMoneyLineKind.Home,
    resultingMoneyLineHome
  ),
  scenarioBetOn<SupportedBetType.MoneyLine>(
    OddsMoneyLineKind.Away,
    resultingMoneyLineAway
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
