import { cond, overEvery, curryRight, flow, gt, eq } from "lodash";
import { SupportedBetType, Bet, OddsMoneyLineKind } from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useFullTimeHomeScore,
  useFullTimeAwayScore,
} from "../cond/score";

const toWinToNilLogic = (
  homeComparisonOp: (value: number, otherValue: number) => boolean,
  awayComparisonOp: (value: number, otherValue: number) => boolean
) =>
  overEvery(
    flow(useFullTimeHomeScore, curryRight(homeComparisonOp)(0)),
    flow(useFullTimeAwayScore, curryRight(awayComparisonOp)(0))
  );

const toWinToNilBetLogic = (
  homeComparisonOp: (value: number, otherValue: number) => boolean,
  awayComparisonOp: (value: number, otherValue: number) => boolean
) =>
  cond([
    [toWinToNilLogic(homeComparisonOp, awayComparisonOp), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const toWinToNilOnPeriodLogic = (
  homeComparisonOp: (value: number, otherValue: number) => boolean,
  awayComparisonOp: (value: number, otherValue: number) => boolean
) =>
  cond([
    [isPeriodFullTime, toWinToNilBetLogic(homeComparisonOp, awayComparisonOp)],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingToWinToNil = cond([
  scenarioBetOn<SupportedBetType.ToWinToNil>(
    OddsMoneyLineKind.Home,
    toWinToNilOnPeriodLogic(gt, eq)
  ),
  scenarioBetOn<SupportedBetType.ToWinToNil>(
    OddsMoneyLineKind.Away,
    toWinToNilOnPeriodLogic(eq, gt)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
