import { cond, flow } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsCleanSheetKind,
  MatchScore,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  throwErrorNotSupportedBetPosition,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  isHomeEqualToZero,
  isHomeGreaterThanZero,
  isAwayEqualToZero,
  isAwayGreaterThanZero,
  useFullTimeScore,
} from "../cond/score";

const compareScore = (compareScoreFn: (data: MatchScore) => boolean) =>
  cond([
    [flow(useFullTimeScore, compareScoreFn), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const cleansheet = (compareScoreFn: (data: MatchScore) => boolean) =>
  cond([
    [isPeriodFullTime, compareScore(compareScoreFn)],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingCleansheet = cond([
  scenarioBetOn<SupportedBetType.CleanSheet>(
    OddsCleanSheetKind.HomeYes,
    cleansheet(isAwayEqualToZero)
  ),
  scenarioBetOn<SupportedBetType.CleanSheet>(
    OddsCleanSheetKind.HomeNo,
    cleansheet(isAwayGreaterThanZero)
  ),
  scenarioBetOn<SupportedBetType.CleanSheet>(
    OddsCleanSheetKind.AwayYes,
    cleansheet(isHomeEqualToZero)
  ),
  scenarioBetOn<SupportedBetType.CleanSheet>(
    OddsCleanSheetKind.AwayNo,
    cleansheet(isHomeGreaterThanZero)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
