import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curryRight,
  eq,
  overEvery,
  overSome,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayTeamToScoreIn1H2HKind,
  OddsBothTeamToScoreIn1H2HKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const scoreCompareToZero = (
  useScore: (data: Bet<any>) => number,
  useComparisonOp: (value: number, othervalue: number) => boolean
) => flow(useScore, curryRight(useComparisonOp)(0));

const yesYesWinLogic = overEvery(
  scoreCompareToZero(useFirstHalfHomeScore, gt),
  scoreCompareToZero(useFirstHalfAwayScore, gt),
  scoreCompareToZero(useHomeSecondHalf, gt),
  scoreCompareToZero(useAwaySecondHalf, gt)
);

const yesNoWinLogic = overEvery(
  scoreCompareToZero(useFirstHalfHomeScore, gt),
  scoreCompareToZero(useFirstHalfAwayScore, gt),
  overSome(
    scoreCompareToZero(useHomeSecondHalf, eq),
    scoreCompareToZero(useAwaySecondHalf, eq)
  )
);

const noYesWinLogic = overEvery(
  overSome(
    scoreCompareToZero(useFirstHalfHomeScore, eq),
    scoreCompareToZero(useFirstHalfAwayScore, eq)
  ),
  scoreCompareToZero(useHomeSecondHalf, gt),
  scoreCompareToZero(useAwaySecondHalf, gt)
);

const noNoWinLogic = overEvery(
  overSome(
    scoreCompareToZero(useFirstHalfHomeScore, eq),
    scoreCompareToZero(useFirstHalfAwayScore, eq)
  ),
  overSome(
    scoreCompareToZero(useHomeSecondHalf, eq),
    scoreCompareToZero(useAwaySecondHalf, eq)
  )
);

const bothTeamToScoreIn1H2HResultingBetStatus = (
  useWinLogic: (data: Bet<any>) => boolean
) =>
  cond([
    [useWinLogic, playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const BothTeamToScoreIn1H2HResulting = (
  useWinLogic: (data: Bet<any>) => boolean
) =>
  cond([
    [isPeriodFullTime, bothTeamToScoreIn1H2HResultingBetStatus(useWinLogic)],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingBothTeamToScoreIn1H2H = cond([
  scenarioBetOn<SupportedBetType.BothTeamtoScoreIn1H_2H>(
    OddsBothTeamToScoreIn1H2HKind.YesYes,
    BothTeamToScoreIn1H2HResulting(yesYesWinLogic)
  ),
  scenarioBetOn<SupportedBetType.BothTeamtoScoreIn1H_2H>(
    OddsBothTeamToScoreIn1H2HKind.YesNo,
    BothTeamToScoreIn1H2HResulting(yesNoWinLogic)
  ),
  scenarioBetOn<SupportedBetType.BothTeamtoScoreIn1H_2H>(
    OddsBothTeamToScoreIn1H2HKind.NoYes,
    BothTeamToScoreIn1H2HResulting(noYesWinLogic)
  ),
  scenarioBetOn<SupportedBetType.BothTeamtoScoreIn1H_2H>(
    OddsBothTeamToScoreIn1H2HKind.NoNo,
    BothTeamToScoreIn1H2HResulting(noNoWinLogic)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
