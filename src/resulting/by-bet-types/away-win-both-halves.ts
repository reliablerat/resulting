import { cond, over, flow, spread, gt, stubFalse, lt } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayWinBothHalvesKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const secondHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) => flow(over(useHomeSecondHalf, useAwaySecondHalf), spread(useOperation));

const firstHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(useFirstHalfHomeScore, useFirstHalfAwayScore),
    spread(useOperation)
  );

const AwayWinBothHalvesResulting = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [
      firstHalfHomeAwayComparison(useOperation),
      secondHalfHomeAwayComparison(useOperation),
    ],
    [otherwise, stubFalse],
  ]);

const resultingAwayWinBothHalvesLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [AwayWinBothHalvesResulting(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingAwayWinBothHalvesOnYes = cond([
  [isPeriodFullTime, resultingAwayWinBothHalvesLogic(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayWinBothHalvesOnNo = cond([
  [isPeriodFullTime, resultingAwayWinBothHalvesLogic(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingAwayWinBothHalves = cond([
  scenarioBetOn<SupportedBetType.AwayWinBothHalves>(
    OddsAwayWinBothHalvesKind.Yes,
    resultingAwayWinBothHalvesOnYes
  ),
  scenarioBetOn<SupportedBetType.AwayWinBothHalves>(
    OddsAwayWinBothHalvesKind.No,
    resultingAwayWinBothHalvesOnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
