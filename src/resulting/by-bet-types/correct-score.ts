import {
  cond,
  CondPair,
  flow,
  eq,
  gt,
  curry,
  curryRight,
  spread,
  over,
  constant,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsCorrectScoreKind,
  MatchScore,
  PlayerBetStatus,
} from "../types";
import { otherwise, throwErrorNotSupportedBetPosition } from "../cond/misc";
import {
  isPeriodFirstHalf,
  isPeriodSecondHalf,
  isPeriodFullTime,
} from "../cond/period";
import { playerBetStatusWin, playerBetStatusLose } from "../cond/bet-status";
import {
  useFirstHalfScore,
  useScore,
  useFullTimeScore,
  usePeriodScore,
} from "../cond/score";

export const isCorrectScoreBetOn = (csKind: OddsCorrectScoreKind) => (
  data: Bet<SupportedBetType.CorrectScore>
): boolean => data.betPosition === csKind;

const csKindScoreToNumbers = (kind: OddsCorrectScoreKind): number[] =>
  kind
    .toString()
    .replace("cs_", "")
    .split("_")
    .map((v) => parseInt(v, 10));

const scoreNumbersToMatchScore = ([home, away]): MatchScore => ({
  home,
  away,
});

const csKindToScore = flow(csKindScoreToNumbers, scoreNumbersToMatchScore);

const isHomeScoreEqual = (score: number) => ({ home }: MatchScore) =>
  eq(home, score);

const isHomeScoreGreatThanFour = (score: number) => ({ home }: MatchScore) =>
  gt(home, score);

const isAwayScoreEqual = (score: number) => ({ away }: MatchScore) =>
  eq(away, score);

const isAwayScoreGreatThanFour = (score: number) => ({ away }: MatchScore) =>
  gt(away, score);

const isScoreEqual = ({ home, away }: MatchScore) =>
  cond([[isHomeScoreEqual(home), isAwayScoreEqual(away)]]);

const isScoreGreatThanFour = ({ home, away }: MatchScore) =>
  cond([[isHomeScoreGreatThanFour(home), isAwayScoreGreatThanFour(away)]]);

const csKindToKindScorePair = (
  kind: OddsCorrectScoreKind
): { kind: OddsCorrectScoreKind; csKindScore: MatchScore } => ({
  kind,
  csKindScore: csKindToScore(kind),
});

const matchScoreByPeriod = (
  usePeriodScoreFn: (data: Bet<SupportedBetType.CorrectScore>) => MatchScore,
  useComparisonFn: ({
    home,
    away,
  }: MatchScore) => (Target: MatchScore) => boolean,
  score: MatchScore
) => {
  return flow(
    usePeriodScoreFn,
    cond([
      [useComparisonFn(score), playerBetStatusWin],
      [otherwise, playerBetStatusLose],
    ])
  );
};

const resultingCorrectScoreByPeriod = (
  useComparisonFn: ({
    home,
    away,
  }: MatchScore) => (Target: MatchScore) => boolean,
  score: MatchScore
) => {
  return cond([
    [
      isPeriodFirstHalf,
      matchScoreByPeriod(useFirstHalfScore, useComparisonFn, score),
    ],
    [isPeriodSecondHalf, matchScoreByPeriod(useScore, useComparisonFn, score)],
    [
      isPeriodFullTime,
      matchScoreByPeriod(useFullTimeScore, useComparisonFn, score),
    ],
  ]);
};

const csKindPairToResultingCond = ({
  kind,
  csKindScore,
}: {
  kind: OddsCorrectScoreKind;
  csKindScore: MatchScore;
}): CondPair<Bet<SupportedBetType.CorrectScore>, PlayerBetStatus> => [
  isCorrectScoreBetOn(kind),
  resultingCorrectScoreByPeriod(isScoreEqual, csKindScore),
];

const allCsKindVariant: OddsCorrectScoreKind[] = [
  OddsCorrectScoreKind.CS0_0,
  OddsCorrectScoreKind.CS1_0,
  OddsCorrectScoreKind.CS0_1,
  OddsCorrectScoreKind.CS2_0,
  OddsCorrectScoreKind.CS0_2,
  OddsCorrectScoreKind.CS2_1,
  OddsCorrectScoreKind.CS1_2,
  OddsCorrectScoreKind.CS3_0,
  OddsCorrectScoreKind.CS0_3,
  OddsCorrectScoreKind.CS3_1,
  OddsCorrectScoreKind.CS1_3,
  OddsCorrectScoreKind.CS3_2,
  OddsCorrectScoreKind.CS2_3,
  OddsCorrectScoreKind.CS4_0,
  OddsCorrectScoreKind.CS0_4,
  OddsCorrectScoreKind.CS4_1,
  OddsCorrectScoreKind.CS1_4,
  OddsCorrectScoreKind.CS4_2,
  OddsCorrectScoreKind.CS2_4,
  OddsCorrectScoreKind.CS4_3,
  OddsCorrectScoreKind.CS3_4,
  OddsCorrectScoreKind.CS1_1,
  OddsCorrectScoreKind.CS2_2,
  OddsCorrectScoreKind.CS3_3,
  OddsCorrectScoreKind.CS4_4,
];

const correctScoreAllBetScenario: CondPair<
  Bet<SupportedBetType.CorrectScore>,
  PlayerBetStatus
>[] = allCsKindVariant
  .map(csKindToKindScorePair)
  .map(csKindPairToResultingCond);

const anyOtherScoreComparison = (bet: Bet<SupportedBetType.CorrectScore>) => {
  const score = usePeriodScore(bet);
  return resultingCorrectScoreByPeriod(isScoreEqual, score)(bet);
};

const anyOtherScoreLogic: CondPair<
  Bet<SupportedBetType.CorrectScore>,
  PlayerBetStatus
> = [
  isCorrectScoreBetOn(OddsCorrectScoreKind.AnyOtherScore),
  anyOtherScoreComparison,
];

export const resultingCorrectScore = cond([
  ...correctScoreAllBetScenario,
  anyOtherScoreLogic,
  [otherwise, throwErrorNotSupportedBetPosition],
]);
