import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curryRight,
  eq,
  overEvery,
  overSome,
  subtract,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayTeamToScoreIn1H2HKind,
  MatchScore,
  OddsBothTeamToScoreKind,
  OddsOverUnderKind,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
  playerBetStatusHalfWin,
  playerBetStatusHalfLose,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFirstHalf, isPeriodSecondHalf } from "../cond/period";
import {
  useHomeScore,
  useAwayScore,
  useFirstHalfScore,
  useSecondHalfScore,
  useFirstHalfHomeScore,
} from "../cond/score";
import { useHandicap } from "../cond/handicap";

const homeHandicapCompare = (
  useOp: (value: number, otherValue: number) => boolean
) => flow(over(useFirstHalfHomeScore, useHandicap), spread(useOp));

const homeHandicapSubtractCompare = (
  useOp: (value: number, otherValue: number) => boolean,
  handicapComparisonValue: number
) =>
  flow(
    over(useFirstHalfHomeScore, useHandicap),
    spread(subtract),
    curryRight(useOp)(handicapComparisonValue)
  );

const homeHandicapAndLogic = (
  leftOp: (value: number, otherValue: number) => boolean,
  rightOp: (value: number, otherValue: number) => boolean,
  handicapComparisonValue: number
) =>
  overEvery(
    homeHandicapCompare(leftOp),
    homeHandicapSubtractCompare(rightOp, handicapComparisonValue)
  );

const homeTeamOverUnderOnOver = cond([
  [homeHandicapAndLogic(gt, eq, 0.25), playerBetStatusHalfWin],
  [homeHandicapAndLogic(gt, gt, 0.25), playerBetStatusWin],
  [homeHandicapCompare(eq), playerBetStatusDraw],
  [homeHandicapAndLogic(lt, eq, -0.25), playerBetStatusHalfLose],
  [homeHandicapAndLogic(lt, lt, -0.25), playerBetStatusLose],
]);

const homeTeamOverUnderOnUnder = cond([
  [homeHandicapAndLogic(lt, eq, -0.25), playerBetStatusHalfWin],
  [homeHandicapAndLogic(lt, lt, -0.25), playerBetStatusWin],
  [homeHandicapCompare(eq), playerBetStatusDraw],
  [homeHandicapAndLogic(gt, eq, 0.25), playerBetStatusHalfLose],
  [homeHandicapAndLogic(gt, gt, 0.25), playerBetStatusLose],
]);

const homeTeamOverUnderResulting = (
  useLogic: (data: Bet<any>) => PlayerBetStatus
) =>
  cond([
    [isPeriodFirstHalf, useLogic],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingHomeTeamOverUnder = cond([
  scenarioBetOn<SupportedBetType.HomeTeamOverUnder>(
    OddsOverUnderKind.Over,
    homeTeamOverUnderResulting(homeTeamOverUnderOnOver)
  ),
  scenarioBetOn<SupportedBetType.HomeTeamOverUnder>(
    OddsOverUnderKind.Under,
    homeTeamOverUnderResulting(homeTeamOverUnderOnUnder)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
