import { cond } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  BetType,
  Odds1x2Kind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin, isDraw } from "../cond/winner";
import {
  otherwise,
  throwErrorNotSupportedBetPosition,
  ErrorNotSupportedBetPosition,
  otherwiseError,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";

const resulting1x2Home = cond([
  [isHomeWin, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const resulting1x2Draw = cond([
  [isDraw, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const resulting1x2Away = cond([
  [isAwayWin, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

export const resulting1x2 = cond([
  scenarioBetOn<BetType.FH1X2 | BetType.FT1X2>(
    Odds1x2Kind.Home,
    resulting1x2Home
  ),
  scenarioBetOn<BetType.FH1X2 | BetType.FT1X2>(
    Odds1x2Kind.Draw,
    resulting1x2Draw
  ),
  scenarioBetOn<BetType.FH1X2 | BetType.FT1X2>(
    Odds1x2Kind.Away,
    resulting1x2Away
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
