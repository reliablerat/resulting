import {
  cond,
  over,
  flow,
  spread,
  gt,
  stubFalse,
  lt,
  curryRight,
  eq,
  overEvery,
  overSome,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsAwayTeamToScoreIn1H2HKind,
  MatchScore,
  OddsBothTeamToScoreKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFirstHalf, isPeriodSecondHalf } from "../cond/period";
import {
  useHomeScore,
  useAwayScore,
  useFirstHalfScore,
  useSecondHalfScore,
} from "../cond/score";

const scoreCompareToZero = (
  useScore: (data: Bet<any>) => number,
  useComparisonOp: (value: number, othervalue: number) => boolean
) => flow(useScore, curryRight(useComparisonOp)(0));

const YesWinLogic = (usePeriod: (data: Bet<any>) => MatchScore) =>
  overEvery(
    scoreCompareToZero(flow(usePeriod, useHomeScore), gt),
    scoreCompareToZero(flow(usePeriod, useAwayScore), gt)
  );

const NoWinLogic = (usePeriod: (data: Bet<any>) => MatchScore) =>
  overSome(
    scoreCompareToZero(flow(usePeriod, useHomeScore), eq),
    scoreCompareToZero(flow(usePeriod, useAwayScore), eq)
  );

const BothTeamToScoreResultingBetStatus = (
  usePeriod: (data: Bet<any>) => MatchScore,
  useWinLogic: (
    usePeriod: (data: Bet<any>) => MatchScore
  ) => (data: Bet<any>) => boolean
) =>
  cond([
    [useWinLogic(usePeriod), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const BothTeamToScoreResulting = (
  useWinLogic: (
    usePeriod: (data: Bet<any>) => MatchScore
  ) => (data: Bet<any>) => boolean
) =>
  cond([
    [
      isPeriodFirstHalf,
      BothTeamToScoreResultingBetStatus(useFirstHalfScore, useWinLogic),
    ],
    [
      isPeriodSecondHalf,
      BothTeamToScoreResultingBetStatus(useSecondHalfScore, useWinLogic),
    ],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

export const resultingBothTeamToScore = cond([
  scenarioBetOn<SupportedBetType.BothTeamtoScore>(
    OddsBothTeamToScoreKind.Yes,
    BothTeamToScoreResulting(YesWinLogic)
  ),
  scenarioBetOn<SupportedBetType.BothTeamtoScore>(
    OddsBothTeamToScoreKind.No,
    BothTeamToScoreResulting(NoWinLogic)
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
