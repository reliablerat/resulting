import { cond, over, flow, spread, gt, stubFalse, lt } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHomeWinBothHalvesKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const secondHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) => flow(over(useHomeSecondHalf, useAwaySecondHalf), spread(useOperation));

const firstHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(useFirstHalfHomeScore, useFirstHalfAwayScore),
    spread(useOperation)
  );

const homeWinBothHalvesResulting = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [
      firstHalfHomeAwayComparison(useOperation),
      secondHalfHomeAwayComparison(useOperation),
    ],
    [otherwise, stubFalse],
  ]);

const resultingHomeWinBothHalvesLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [homeWinBothHalvesResulting(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHomeWinBothHalvesOnYes = cond([
  [isPeriodFullTime, resultingHomeWinBothHalvesLogic(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHomeWinBothHalvesOnNo = cond([
  [isPeriodFullTime, resultingHomeWinBothHalvesLogic(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingHomeWinBothHalves = cond([
  scenarioBetOn<SupportedBetType.HomeWinBothHalves>(
    OddsHomeWinBothHalvesKind.Yes,
    resultingHomeWinBothHalvesOnYes
  ),
  scenarioBetOn<SupportedBetType.HomeWinBothHalves>(
    OddsHomeWinBothHalvesKind.No,
    resultingHomeWinBothHalvesOnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
