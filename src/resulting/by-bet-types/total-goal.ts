import { cond, map, CondPair, flow, constant } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsTotalGoalKind,
  PlayerBetStatus,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  throwErrorNotSupportedBetPosition,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import {
  isPeriodFullTime,
  isPeriodFirstHalf,
  isPeriodSecondHalf,
} from "../cond/period";
import { inRange } from "lodash";
import {
  combineFirstHalfScore,
  combineSecondHalfScore,
  combineFullTimeScore,
} from "../cond/score";

const isTotalGoalBetPosition0to1 = (data: Bet<SupportedBetType.TotalGoal>) =>
  data.betPosition === OddsTotalGoalKind._0_1;

const isTotalGoalBetPosition2to3 = (data: Bet<SupportedBetType.TotalGoal>) =>
  data.betPosition === OddsTotalGoalKind._2_3;

const isTotalGoalBetPosition4AndAbove = (
  data: Bet<SupportedBetType.TotalGoal>
) => data.betPosition === OddsTotalGoalKind._4AndAbove;

const isTotalGoalBetPosition4To6 = (data: Bet<SupportedBetType.TotalGoal>) =>
  data.betPosition === OddsTotalGoalKind._4_6;

const isTotalGoalBetPosition7AndAbove = (
  data: Bet<SupportedBetType.TotalGoal>
) => data.betPosition === OddsTotalGoalKind._7AndAbove;

// condition logic here
const isTotalGoalInBetween = (
  combineScore: (data: Bet<any>) => number,
  min: number,
  max: number = Infinity
) =>
  cond([
    [
      flow(combineScore, (v: number) => v >= min && v <= max),
      playerBetStatusWin,
    ],
    [otherwise, playerBetStatusLose],
  ]);

// resulting logic here
const resultingTotalGoal0To1 = cond([
  [isPeriodFirstHalf, isTotalGoalInBetween(combineFirstHalfScore, 0, 1)],
  [isPeriodSecondHalf, isTotalGoalInBetween(combineSecondHalfScore, 0, 1)],
  [isPeriodFullTime, isTotalGoalInBetween(combineFirstHalfScore, 0, 1)],
]);

const resultingTotalGoal2To3 = cond([
  [isPeriodFirstHalf, isTotalGoalInBetween(combineFirstHalfScore, 2, 3)],
  [isPeriodSecondHalf, isTotalGoalInBetween(combineSecondHalfScore, 2, 3)],
  [isPeriodFullTime, isTotalGoalInBetween(combineFullTimeScore, 2, 3)],
]);

const resultingTotalGoal4AndAbove = cond([
  [isPeriodFirstHalf, isTotalGoalInBetween(combineFirstHalfScore, 4)],
  [isPeriodSecondHalf, isTotalGoalInBetween(combineSecondHalfScore, 4)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingTotalGoal4To6 = cond([
  [isPeriodFullTime, isTotalGoalInBetween(combineFullTimeScore, 4, 6)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingTotalGoal7AndAbove = cond([
  [isPeriodFullTime, isTotalGoalInBetween(combineFullTimeScore, 7)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingTotalGoal = cond([
  [isTotalGoalBetPosition0to1, resultingTotalGoal0To1],
  [isTotalGoalBetPosition2to3, resultingTotalGoal2To3],
  [isTotalGoalBetPosition4AndAbove, resultingTotalGoal4AndAbove],
  [isTotalGoalBetPosition4To6, resultingTotalGoal4To6],
  [isTotalGoalBetPosition7AndAbove, resultingTotalGoal7AndAbove],
  [otherwise, throwErrorNotSupportedBetPosition],
]);
