import { cond, over, flow, spread, gt, stubFalse, lt, stubTrue } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHomeWinEitherHalvesKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useHomeSecondHalf,
  useAwaySecondHalf,
  useFirstHalfHomeScore,
  useFirstHalfAwayScore,
} from "../cond/score";

const secondHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) => flow(over(useHomeSecondHalf, useAwaySecondHalf), spread(useOperation));

const firstHalfHomeAwayComparison = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(useFirstHalfHomeScore, useFirstHalfAwayScore),
    spread(useOperation)
  );

const homeWinEitherHalvesResulting = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [firstHalfHomeAwayComparison(useOperation), stubTrue],
    [secondHalfHomeAwayComparison(useOperation), stubTrue],
    [otherwise, stubFalse],
  ]);

const resultingHomeWinEitherHalvesLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [homeWinEitherHalvesResulting(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHomeWinEitherHalvesOnYes = cond([
  [isPeriodFullTime, resultingHomeWinEitherHalvesLogic(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHomeWinEitherHalvesOnNo = cond([
  [isPeriodFullTime, resultingHomeWinEitherHalvesLogic(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingHomeWinEitherHalves = cond([
  scenarioBetOn<SupportedBetType.HomeWinEitherHalves>(
    OddsHomeWinEitherHalvesKind.Yes,
    resultingHomeWinEitherHalvesOnYes
  ),
  scenarioBetOn<SupportedBetType.HomeWinEitherHalves>(
    OddsHomeWinEitherHalvesKind.No,
    resultingHomeWinEitherHalvesOnNo
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
