import { cond, flow, over, spread, subtract, lt, gt, eq } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHighestScoringHalfKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useFullTimeScore,
  useCombineHomeAwayScore,
  combineFullTimeScore,
  combineFirstHalfScore,
  useHomeScore,
  useFirstHalfScore,
  useFullTimeHomeScore,
  useFirstHalfAwayScore,
  useFullTimeAwayScore,
} from "../cond/score";

const highestScoringHalfAwayLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(
      flow(over(useFullTimeAwayScore, useFirstHalfAwayScore), spread(subtract)),
      useFirstHalfAwayScore
    ),
    spread(useOperation)
  );

const highestScoringHalfAwayCond = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [highestScoringHalfAwayLogic(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHighestScoringHalfAwayFirstHalf = cond([
  [isPeriodFullTime, highestScoringHalfAwayCond(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHighestScoringHalfAwaySecondHalf = cond([
  [isPeriodFullTime, highestScoringHalfAwayCond(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHighestScoringHalfAwayTied = cond([
  [isPeriodFullTime, highestScoringHalfAwayCond(eq)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingHighestScoringHalfAway = cond([
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.FirstHalf,
    resultingHighestScoringHalfAwayFirstHalf
  ),
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.SecondHalf,
    resultingHighestScoringHalfAwaySecondHalf
  ),
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.Tie,
    resultingHighestScoringHalfAwayTied
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
