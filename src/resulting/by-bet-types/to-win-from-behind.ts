import {
  cond,
  overEvery,
  curryRight,
  flow,
  gt,
  eq,
  over,
  spread,
  lt,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  MatchGoalSequence,
  GoalSequence,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useFullTimeHomeScore,
  useFullTimeAwayScore,
} from "../cond/score";

const useFullTimeGoalSequence = (data: Bet<any>): MatchGoalSequence =>
  data.goalSequence.fullTime;
const useFirstGoal = (goalSequence: MatchGoalSequence): GoalSequence =>
  goalSequence.firstGoal;

const bb = (homeOrAwaySeq: GoalSequence) =>
  flow(useFullTimeGoalSequence, useFirstGoal, curryRight(eq)(homeOrAwaySeq));

const cc = (
  homeOrAwaySeq: GoalSequence,
  useOpScore: (value: number, otherValue: number) => boolean
) =>
  overEvery(
    bb(homeOrAwaySeq),
    flow(over(useFullTimeHomeScore, useFullTimeAwayScore), spread(useOpScore))
  );

const toWinFromBehindLogic = ({
  firstSeqLose,
  firstSeqWin,
  useOpScore,
}: {
  firstSeqLose: GoalSequence;
  firstSeqWin: GoalSequence;
  useOpScore: (value: number, otherValue: number) => boolean;
}) =>
  cond([
    [bb(firstSeqLose), playerBetStatusLose],
    [cc(firstSeqWin, useOpScore), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingToWinFromBehindOnHome = cond([
  [
    isPeriodFullTime,
    toWinFromBehindLogic({
      firstSeqLose: GoalSequence.HomeFirst,
      firstSeqWin: GoalSequence.AwayFirst,
      useOpScore: gt,
    }),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingToWinFromBehindOnAway = cond([
  [
    isPeriodFullTime,
    toWinFromBehindLogic({
      firstSeqLose: GoalSequence.AwayFirst,
      firstSeqWin: GoalSequence.HomeFirst,
      useOpScore: lt,
    }),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingToWinFromBehind = cond([
  scenarioBetOn<SupportedBetType.ToWinFromBehind>(
    OddsMoneyLineKind.Home,
    resultingToWinFromBehindOnHome
  ),
  scenarioBetOn<SupportedBetType.ToWinFromBehind>(
    OddsMoneyLineKind.Away,
    resultingToWinFromBehindOnAway
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
