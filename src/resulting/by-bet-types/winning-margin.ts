import {
  cond,
  flow,
  over,
  spread,
  eq,
  curry,
  curryRight,
  gte,
  stubTrue,
  stubFalse,
  constant,
  overEvery,
} from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsWinningMarginKind,
  MatchScore,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import { isHomeWin, isAwayWin } from "../cond/winner";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFirstHalf, isPeriodFullTime } from "../cond/period";
import {
  useFirstHalfScore,
  useSubtractHomeAwayScore,
  useFullTimeScore,
  useSubtractAwayHomeScore,
  isHomeGreaterThanZero,
  isAwayGreaterThanZero,
  isHomeEqualToZero,
  isAwayEqualToZero,
} from "../cond/score";

const resultWithLeadLogic = ({
  leadNumber,
  usePeriodScore,
  useOperator,
  useSubtractLogic,
}: {
  leadNumber: number;
  usePeriodScore: (data: Bet<any>) => MatchScore;
  useOperator: (value: number, othervalue: number) => boolean;
  useSubtractLogic: (score: MatchScore) => number;
}) =>
  cond([
    [
      flow(
        usePeriodScore,
        useSubtractLogic,
        curryRight(useOperator)(leadNumber)
      ),
      playerBetStatusWin,
    ],
    [otherwise, playerBetStatusLose],
  ]);

const homeLeadParams = {
  usePeriodScore: useFirstHalfScore,
  useOperator: eq,
  useSubtractLogic: useSubtractHomeAwayScore,
  leadNumber: 1,
};

const awayLeadParams = {
  ...homeLeadParams,
  useSubtractLogic: useSubtractAwayHomeScore,
};

const resultingHomeWin1 = cond([
  [isPeriodFirstHalf, resultWithLeadLogic(homeLeadParams)],
  [
    isPeriodFullTime,
    resultWithLeadLogic({
      ...homeLeadParams,
      usePeriodScore: useFullTimeScore,
    }),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayWin1 = cond([
  [isPeriodFirstHalf, resultWithLeadLogic(awayLeadParams)],
  [
    isPeriodFullTime,
    resultWithLeadLogic({
      ...awayLeadParams,
      usePeriodScore: useFullTimeScore,
    }),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHomeWin2 = cond([
  [
    isPeriodFirstHalf,
    resultWithLeadLogic({ ...homeLeadParams, leadNumber: 2 }),
  ],
  [
    isPeriodFullTime,
    resultWithLeadLogic({
      ...homeLeadParams,
      usePeriodScore: useFullTimeScore,
      leadNumber: 2,
    }),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayWin2 = cond([
  [
    isPeriodFirstHalf,
    resultWithLeadLogic({ ...awayLeadParams, leadNumber: 2 }),
  ],
  [
    isPeriodFullTime,
    resultWithLeadLogic({
      ...awayLeadParams,
      usePeriodScore: useFullTimeScore,
      leadNumber: 2,
    }),
  ],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHomeWin2Up = cond([
  [
    isPeriodFirstHalf,
    resultWithLeadLogic({ ...homeLeadParams, useOperator: gte, leadNumber: 2 }),
  ],

  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayWin2Up = cond([
  [
    isPeriodFirstHalf,
    resultWithLeadLogic({ ...awayLeadParams, useOperator: gte, leadNumber: 2 }),
  ],

  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHomeWin3Up = cond([
  [
    isPeriodFullTime,
    resultWithLeadLogic({
      ...homeLeadParams,
      usePeriodScore: useFullTimeScore,
      useOperator: gte,
      leadNumber: 3,
    }),
  ],

  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingAwayWin3Up = cond([
  [
    isPeriodFullTime,
    resultWithLeadLogic({
      ...awayLeadParams,
      usePeriodScore: useFullTimeScore,
      useOperator: gte,
      leadNumber: 3,
    }),
  ],

  [otherwise, throwErrorNotSupportedPeriod],
]);

const drawLogic = (usePeriodScore: (data: Bet<any>) => MatchScore) =>
  flow(
    usePeriodScore,
    cond([
      [
        overEvery([
          isHomeGreaterThanZero,
          isAwayGreaterThanZero,
          flow(useSubtractHomeAwayScore, curry(eq)(0)),
        ]),
        playerBetStatusWin,
      ],
      [otherwise, playerBetStatusLose],
    ])
  );

const resultingOtherDraw = cond([
  [isPeriodFirstHalf, drawLogic(useFirstHalfScore)],
  [isPeriodFullTime, drawLogic(useFullTimeScore)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const noGoalLogic = (usePeriodScore: (data: Bet<any>) => MatchScore) =>
  flow(
    usePeriodScore,
    cond([
      [overEvery([isHomeEqualToZero, isAwayEqualToZero]), playerBetStatusWin],
      [otherwise, playerBetStatusLose],
    ])
  );

const resultingNoGoal = cond([
  [isPeriodFirstHalf, noGoalLogic(useFirstHalfScore)],
  [isPeriodFullTime, noGoalLogic(useFullTimeScore)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingWinningMargin = cond([
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.HomeLeads1,
    resultingHomeWin1
  ),
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.HomeLeads2,
    resultingHomeWin2
  ),
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.HomeLeads2Up,
    resultingHomeWin2Up
  ),
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.HomeLeads3Up,
    resultingHomeWin3Up
  ),

  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.AwayLeads1,
    resultingAwayWin1
  ),
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.AwayLeads2,
    resultingAwayWin2
  ),
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.AwayLeads2Up,
    resultingAwayWin2Up
  ),
  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.AwayLeads3Up,
    resultingAwayWin3Up
  ),

  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.OtherDraw,
    resultingOtherDraw
  ),

  scenarioBetOn<SupportedBetType.WinningMargin>(
    OddsWinningMarginKind.NoGoal,
    resultingNoGoal
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
