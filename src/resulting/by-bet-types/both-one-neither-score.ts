import { cond, flow, gt, property, eq, constant } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsBothOneNeitherToScoreKind,
  MatchScore,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  throwErrorNotSupportedBetPosition,
  otherwiseError,
  ErrorNotSupportedBetPosition,
  ErrorHandicapValueNotSupported,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { isBetPositionOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  isHomeGreaterThanZero,
  isAwayGreaterThanZero,
  isAwayEqualToZero,
  isHomeEqualToZero,
  useFullTimeScore,
} from "../cond/score";

const isBoth = cond([[isHomeGreaterThanZero, isAwayGreaterThanZero]]);
const isOne = cond([
  [cond([[isHomeGreaterThanZero, isAwayEqualToZero]]), constant(true)],
  [cond([[isHomeEqualToZero, isAwayGreaterThanZero]]), constant(true)],
  [otherwise, constant(false)],
]);
const isNeither = cond([[isHomeEqualToZero, isAwayEqualToZero]]);

const useLogic = (comparisonFn: (data: MatchScore) => boolean) =>
  flow(
    useFullTimeScore,
    cond([
      [comparisonFn, playerBetStatusWin],
      [otherwise, playerBetStatusLose],
    ])
  );

const resultingOneNeitherToScoreBoth = cond([
  [isPeriodFullTime, useLogic(isBoth)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingOneNeitherToScoreOne = cond([
  [isPeriodFullTime, useLogic(isOne)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingOneNeitherToScoreNeither = cond([
  [isPeriodFullTime, useLogic(isNeither)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingBothOneNeitherScore = cond([
  [
    isBetPositionOn<SupportedBetType.BothOneNeithertoScore>(
      OddsBothOneNeitherToScoreKind.Both
    ),
    resultingOneNeitherToScoreBoth,
  ],

  [
    isBetPositionOn<SupportedBetType.BothOneNeithertoScore>(
      OddsBothOneNeitherToScoreKind.One
    ),
    resultingOneNeitherToScoreOne,
  ],

  [
    isBetPositionOn<SupportedBetType.BothOneNeithertoScore>(
      OddsBothOneNeitherToScoreKind.Neither
    ),
    resultingOneNeitherToScoreNeither,
  ],
  otherwiseError(ErrorNotSupportedBetPosition),
]);
