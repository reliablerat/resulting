import { cond } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHalfTimeFullTimeKind,
  OddsHalfTimeFullTimeOddEvenKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  isHomeWin,
  isAwayWin,
  isHomeWinFirstHalf,
  isHomeWinFullTime,
  isDrawFulltime,
  isAwayWinFullTime,
  isDrawFirstHalf,
  isAwayWinFirstHalf,
} from "../cond/winner";
import {
  otherwise,
  throwErrorNotSupportedBetPosition,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { isPeriodFullTime } from "../cond/period";
import {
  isFirstHalfCombineScoreOdd,
  isFulltimeCombineScoreOdd,
  isFulltimeCombineScoreEven,
  isFirstHalfCombineScoreEven,
} from "./odd-even";
import { isBetPositionOn } from "../cond/bet-position";

const expectHTFTOddEven = (
  firstHalfOddEven: (data: Bet<any>) => boolean,
  fulltimeOddEven: (data: Bet<any>) => boolean
) =>
  cond([
    [cond([[firstHalfOddEven, fulltimeOddEven]]), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHTFTOE = (
  firstHalfOddEven: (data: Bet<any>) => boolean,
  fulltimeOddEven: (data: Bet<any>) => boolean
) =>
  cond([
    [isPeriodFullTime, expectHTFTOddEven(firstHalfOddEven, fulltimeOddEven)],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

const resultingHTFTOddOdd = resultingHTFTOE(
  isFirstHalfCombineScoreOdd,
  isFulltimeCombineScoreOdd
);

const resultingHTFTOddEven = resultingHTFTOE(
  isFirstHalfCombineScoreOdd,
  isFulltimeCombineScoreEven
);

const resultingHTFTEvenOdd = resultingHTFTOE(
  isFirstHalfCombineScoreEven,
  isFulltimeCombineScoreOdd
);

const resultingHTFTEvenEven = resultingHTFTOE(
  isFirstHalfCombineScoreEven,
  isFulltimeCombineScoreEven
);

export const resultingHalfTimeFullTimeOddEven = cond([
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTimeOdd_Even>(
      OddsHalfTimeFullTimeOddEvenKind.OddOdd
    ),
    resultingHTFTOddOdd,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTimeOdd_Even>(
      OddsHalfTimeFullTimeOddEvenKind.OddEven
    ),
    resultingHTFTOddEven,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTimeOdd_Even>(
      OddsHalfTimeFullTimeOddEvenKind.EvenOdd
    ),
    resultingHTFTEvenOdd,
  ],
  [
    isBetPositionOn<SupportedBetType.HalfTime_FullTimeOdd_Even>(
      OddsHalfTimeFullTimeOddEvenKind.EvenEven
    ),
    resultingHTFTEvenEven,
  ],
  [otherwise, throwErrorNotSupportedBetPosition],
]);
