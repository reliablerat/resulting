import { cond, eq, flow, subtract } from "lodash";
import { SupportedBetType, Bet, OddsOddEvenKind } from "../types";
import {
  isPeriodFirstHalf,
  isPeriodSecondHalf,
  isPeriodFullTime,
} from "../cond/period";
import { isEven, isOdd } from "../cond/number";
import { playerBetStatusWin, playerBetStatusLose } from "../cond/bet-status";
import { otherwise, throwErrorNotSupportedBetPosition } from "../cond/misc";
import {
  combineFirstHalfScore,
  combineFullTimeScore,
  combineSecondHalfScore,
} from "../cond/score";

const isOddEvenBetPositionOdd = ({
  betPosition,
}: Bet<SupportedBetType.OddEven>) => eq(betPosition, OddsOddEvenKind.Odd);

const isOddEvenBetPositionEven = ({
  betPosition,
}: Bet<SupportedBetType.OddEven>) => eq(betPosition, OddsOddEvenKind.Even);

export const isFirstHalfCombineScoreOdd = flow(combineFirstHalfScore, isOdd);
export const isFirstHalfCombineScoreEven = flow(combineFirstHalfScore, isEven);
export const isFulltimeCombineScoreOdd = flow(combineFullTimeScore, isOdd);
export const isFulltimeCombineScoreEven = flow(combineFullTimeScore, isEven);
export const isSecondHalfCombineScoreOdd = flow(combineSecondHalfScore, isOdd);
export const isSecondHalfCombineScoreEven = flow(
  combineSecondHalfScore,
  isEven
);

const isCombinedScoreOdd = cond<Bet<SupportedBetType.OddEven>, boolean>([
  [isPeriodFirstHalf, isFirstHalfCombineScoreOdd],
  [isPeriodSecondHalf, isSecondHalfCombineScoreOdd],
  [isPeriodFullTime, isFulltimeCombineScoreOdd],
]);

const isCombinedScoreEven = cond([
  [isPeriodFirstHalf, isFirstHalfCombineScoreEven],
  [isPeriodSecondHalf, isSecondHalfCombineScoreEven],
  [isPeriodFullTime, isFulltimeCombineScoreEven],
]);

const resultingOddEvenBetOnOdd = cond([
  [isCombinedScoreOdd, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const resultingOddEvenBetOnEven = cond([
  [isCombinedScoreEven, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

export const resultingOddEven = cond([
  [isOddEvenBetPositionOdd, resultingOddEvenBetOnOdd],
  [isOddEvenBetPositionEven, resultingOddEvenBetOnEven],
  [otherwise, throwErrorNotSupportedBetPosition],
]);
