import { cond, flow, over, spread, subtract, lt, gt, eq } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsHighestScoringHalfKind,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import { isPeriodFullTime } from "../cond/period";
import {
  useFullTimeScore,
  useCombineHomeAwayScore,
  combineFullTimeScore,
  combineFirstHalfScore,
} from "../cond/score";

const highestScoringHalfLogic = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  flow(
    over(
      flow(over(combineFullTimeScore, combineFirstHalfScore), spread(subtract)),
      combineFirstHalfScore
    ),
    spread(useOperation)
  );

const highestScoringHalfCond = (
  useOperation: (value: number, otherValue: number) => boolean
) =>
  cond([
    [highestScoringHalfLogic(useOperation), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

const resultingHighestScoringHalfFirstHalf = cond([
  [isPeriodFullTime, highestScoringHalfCond(lt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHighestScoringHalfSecondHalf = cond([
  [isPeriodFullTime, highestScoringHalfCond(gt)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

const resultingHighestScoringHalfTied = cond([
  [isPeriodFullTime, highestScoringHalfCond(eq)],
  [otherwise, throwErrorNotSupportedPeriod],
]);

export const resultingHighestScoringHalf = cond([
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.FirstHalf,
    resultingHighestScoringHalfFirstHalf
  ),
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.SecondHalf,
    resultingHighestScoringHalfSecondHalf
  ),
  scenarioBetOn<SupportedBetType.HighestScoringHalf>(
    OddsHighestScoringHalfKind.Tie,
    resultingHighestScoringHalfTied
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
