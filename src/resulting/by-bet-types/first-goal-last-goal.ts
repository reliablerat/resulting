import { cond, flow } from "lodash";
import {
  SupportedBetType,
  Bet,
  OddsMoneyLineKind,
  OddsFirstGoalLastGoalKind,
  GoalSequence,
  MatchGoalSequence,
} from "../types";
import { playerBetStatusWin, playerBetStatusLose } from "../cond/bet-status";
import { otherwise, throwErrorNotSupportedBetPosition } from "../cond/misc";
import {
  isPeriodFirstHalf,
  isPeriodSecondHalf,
  isPeriodFullTime,
} from "../cond/period";
import { isBetPositionOn } from "../cond/bet-position";

const useFirstHalfGoalSeq = (
  data: Bet<SupportedBetType.FirstGoalAndLastGoal>
) => data.goalSequence.firstHalf;

const useSecondHalfGoalSeq = (
  data: Bet<SupportedBetType.FirstGoalAndLastGoal>
) => data.goalSequence.secondHalf;

const useFullTimeGoalSeq = (data: Bet<SupportedBetType.FirstGoalAndLastGoal>) =>
  data.goalSequence.fullTime;

const usePositionFirstGoal = (seq: MatchGoalSequence) => seq.firstGoal;
const usePositionLastGoal = (seq: MatchGoalSequence) => seq.lastGoal;
const goalSequenceCompare = (b: GoalSequence) => (a: GoalSequence) => a === b;

const isGoalOnPositionAndCompareMatch = (
  goalSeqPeriod: (
    data: Bet<SupportedBetType.FirstGoalAndLastGoal>
  ) => MatchGoalSequence,
  goalSequencePosition: (seq: MatchGoalSequence) => GoalSequence,
  compare: GoalSequence
) =>
  flow<
    Bet<SupportedBetType.FirstGoalAndLastGoal>[],
    MatchGoalSequence,
    GoalSequence,
    boolean
  >(goalSeqPeriod, goalSequencePosition, goalSequenceCompare(compare));

const isAaa = (
  goalSequencePosition: (seq: MatchGoalSequence) => GoalSequence,
  compare: GoalSequence
) =>
  cond([
    [
      isPeriodFirstHalf,
      isGoalOnPositionAndCompareMatch(
        useFirstHalfGoalSeq,
        goalSequencePosition,
        compare
      ),
    ],
    [
      isPeriodSecondHalf,
      isGoalOnPositionAndCompareMatch(
        useSecondHalfGoalSeq,
        goalSequencePosition,
        compare
      ),
    ],
    [
      isPeriodFullTime,
      isGoalOnPositionAndCompareMatch(
        useFullTimeGoalSeq,
        goalSequencePosition,
        compare
      ),
    ],
  ]);

const resulting = (
  goalSequencePosition: (seq: MatchGoalSequence) => GoalSequence,
  compare: GoalSequence
) =>
  cond([
    [isAaa(goalSequencePosition, compare), playerBetStatusWin],
    [otherwise, playerBetStatusLose],
  ]);

// resulting here

const resultingHomeFirst = resulting(
  usePositionFirstGoal,
  GoalSequence.HomeFirst
);

const resultingHomeLast = resulting(usePositionLastGoal, GoalSequence.HomeLast);
const resultingAwayFirst = resulting(
  usePositionFirstGoal,
  GoalSequence.AwayFirst
);
const resultingAwayLast = resulting(usePositionLastGoal, GoalSequence.AwayLast);
const resultingNoGoal = resulting(usePositionFirstGoal, GoalSequence.NoGoal);

export const resultingFirstGoalLastGoal = cond([
  [
    isBetPositionOn<SupportedBetType.FirstGoalAndLastGoal>(
      OddsFirstGoalLastGoalKind.HomeFirst
    ),
    resultingHomeFirst,
  ],
  [
    isBetPositionOn<SupportedBetType.FirstGoalAndLastGoal>(
      OddsFirstGoalLastGoalKind.HomeLast
    ),
    resultingHomeLast,
  ],
  [
    isBetPositionOn<SupportedBetType.FirstGoalAndLastGoal>(
      OddsFirstGoalLastGoalKind.AwayFirst
    ),
    resultingAwayFirst,
  ],
  [
    isBetPositionOn<SupportedBetType.FirstGoalAndLastGoal>(
      OddsFirstGoalLastGoalKind.AwayLast
    ),
    resultingAwayLast,
  ],
  [
    isBetPositionOn<SupportedBetType.FirstGoalAndLastGoal>(
      OddsFirstGoalLastGoalKind.NoGoal
    ),
    resultingNoGoal,
  ],
  [otherwise, throwErrorNotSupportedBetPosition],
]);
