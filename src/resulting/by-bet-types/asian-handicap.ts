import { cond, flow, curry, eq, gt, curryRight } from "lodash";
import {
  BetType,
  OddsAsianHandicapKind,
  PlayerBetStatus,
  MatchScore,
  Bet,
} from "../types";
import {
  playerBetStatusWin,
  playerBetStatusLose,
  playerBetStatusHalfLose,
  playerBetStatusHalfWin,
  playerBetStatusDraw,
} from "../cond/bet-status";
import {
  otherwise,
  ErrorNotSupportedBetPosition,
  otherwiseError,
  throwErrorNotSupportedPeriod,
  throwErrorNotSupportedHandicap,
} from "../cond/misc";
import { scenarioBetOn } from "../cond/bet-position";
import {
  isHandicap_0_25,
  isHandicap_0_75,
  isHandicap_0_50,
  isHandicap_1_00,
} from "../../resulting/cond/handicap";
import {
  isPeriodFirstHalf,
  isPeriodFullTime,
} from "../../resulting/cond/period";
import {
  useFirstHalfScore,
  useSubtractHomeAwayScore,
  useActualScore,
  useFullTimeScore,
} from "../../resulting/cond/score";
import {
  isHomeWinOnActualScore,
  isAwayWinOnActualScore,
  isDrawOnActualScore,
} from "../cond/winner";

export type QuarterHandicapParams = {
  handicapCompare: number;
  eqPlayerBetStatus: () => PlayerBetStatus;
  gtPlayerBetStatus: () => PlayerBetStatus;
  otherPlayerBetStatus: () => PlayerBetStatus;
};

const quarterHandicapCompareResult = ({
  handicapCompare,
  eqPlayerBetStatus,
  gtPlayerBetStatus,
  otherPlayerBetStatus,
}: QuarterHandicapParams) =>
  cond([
    [curry(eq)(handicapCompare), eqPlayerBetStatus],
    [curryRight(gt)(handicapCompare), gtPlayerBetStatus],
    [otherwise, otherPlayerBetStatus],
  ]);

const home025Params: QuarterHandicapParams = {
  handicapCompare: -0.25,
  eqPlayerBetStatus: playerBetStatusHalfLose,
  gtPlayerBetStatus: playerBetStatusWin,
  otherPlayerBetStatus: playerBetStatusLose,
};

const away025Params: QuarterHandicapParams = {
  handicapCompare: -0.25,
  eqPlayerBetStatus: playerBetStatusHalfWin,
  gtPlayerBetStatus: playerBetStatusLose,
  otherPlayerBetStatus: playerBetStatusWin,
};

const quarterHandicapByPeriodFlow = (
  usePeriod: (data: Bet<any>) => MatchScore,
  quarterParams: QuarterHandicapParams
) => {
  const c = (data) => {
    console.log({
      data,
      quarterParams,
      a: quarterParams.eqPlayerBetStatus(),
      b: quarterParams.gtPlayerBetStatus(),
      c: quarterParams.otherPlayerBetStatus(),
    });
    return data;
  };

  return flow(
    useActualScore(usePeriod),
    c,
    useSubtractHomeAwayScore,
    c,
    quarterHandicapCompareResult(quarterParams),
    c
  );
};

const quarterHandicapResult = (quarterParams: QuarterHandicapParams) =>
  cond([
    [
      isPeriodFirstHalf,
      quarterHandicapByPeriodFlow(useFirstHalfScore, quarterParams),
    ],
    [
      isPeriodFullTime,
      quarterHandicapByPeriodFlow(useFullTimeScore, quarterParams),
    ],
    [otherwise, throwErrorNotSupportedPeriod],
  ]);

const halfHandicapHomeResult = cond([
  [isHomeWinOnActualScore, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const halfHandicapAwayResult = cond([
  [isAwayWinOnActualScore, playerBetStatusWin],
  [otherwise, playerBetStatusLose],
]);

const fullHandicapHomeResult = cond([
  [isHomeWinOnActualScore, playerBetStatusWin],
  [isDrawOnActualScore, playerBetStatusDraw],
  [otherwise, playerBetStatusLose],
]);

const fullHandicapAwayResult = cond([
  [isAwayWinOnActualScore, playerBetStatusWin],
  [isDrawOnActualScore, playerBetStatusDraw],
  [otherwise, playerBetStatusLose],
]);

const resultingAsianHandicapHome = cond([
  [isHandicap_0_25, quarterHandicapResult(home025Params)],
  [isHandicap_0_50, halfHandicapHomeResult],
  [
    isHandicap_0_75,
    quarterHandicapResult({
      ...home025Params,
      eqPlayerBetStatus: playerBetStatusHalfWin,
      handicapCompare: 0.25,
    }),
  ],
  [isHandicap_1_00, fullHandicapHomeResult],
  [otherwise, throwErrorNotSupportedHandicap],
]);

const resultingAsianHandicapAway = cond([
  [isHandicap_0_25, quarterHandicapResult(away025Params)],
  [isHandicap_0_50, halfHandicapAwayResult],
  [
    isHandicap_0_75,
    quarterHandicapResult({
      ...away025Params,
      eqPlayerBetStatus: playerBetStatusHalfLose,
      handicapCompare: 0.25,
    }),
  ],
  [isHandicap_1_00, fullHandicapAwayResult],
  [otherwise, throwErrorNotSupportedHandicap],
]);

export const resultingAsianHandicap = cond([
  scenarioBetOn<BetType.FHAH | BetType.FTAH>(
    OddsAsianHandicapKind.Home,
    resultingAsianHandicapHome
  ),
  scenarioBetOn<BetType.FHAH | BetType.FTAH>(
    OddsAsianHandicapKind.Away,
    resultingAsianHandicapAway
  ),
  otherwiseError(ErrorNotSupportedBetPosition),
]);
