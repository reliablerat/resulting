export enum BetType {
  FTAH,
  FTOU,
  FHAH,
  FHOU,
  FT1X2,
  FH1X2,
}

export enum SupportedBetType {
  AsianHandicap = 101,
  OverUnder = 102,
  _1X2 = 103,
  MoneyLine = 104,
  OddEven = 105,
  DoubleChance = 106,
  TotalGoal = 107,
  HalfTime_FullTime = 108,
  HalfTime_FullTimeOdd_Even = 109,
  FirstGoalAndLastGoal = 110,
  CorrectScore = 111,
  BothOneNeithertoScore = 112,
  ThreeWayHandicap = 113,
  CleanSheet = 114,
  ToWinToNil = 115,
  ToWinFromBehind = 116,
  WinningMargin = 117,
  HighestScoringHalf = 118,
  HighestScoringHalfHome = 119,
  HighestScoringHalfAway = 120,
  HomeWinBothHalves = 121,
  AwayWinBothHalves = 122,
  HomeWinEitherHalves = 123,
  AwayWinEitherHalves = 124,
  HomeScoreBothHalves = 125,
  AwayScoreBothHalves = 126,
  BothHalvesOver1_5 = 127,
  BothHalvesUnder1_5 = 128,
  BothTeamtoScoreIn1H_2H = 129,
  Home1HtoScore_2HtoScore = 130,
  Away1HtoScore_2HtoScore = 131,
  BothTeamtoScore = 132,
  HomeTeamOverUnder = 133,
  AwayTeamOverUnder = 134,
}

export enum Period {
  firstHalf = "firstHalf",
  secondHalf = "secondHalf",
  fullTime = "fullTime",
}

export enum GoalSequence {
  HomeFirst = "home_first",
  HomeLast = "home_last",
  AwayFirst = "away_first",
  AwayLast = "away_last",
  NoGoal = "no_goal",
}

export type MatchGoalSequence = {
  firstGoal: GoalSequence;
  lastGoal: GoalSequence;
};

export enum FavouriteType {
  None,
  Home,
  Away,
}

export type Bet<T> = {
  betCode: T;
  period: Period;
  betPosition: OddsKind<T>;
  handicap: number;
  favType: FavouriteType;
  score: ScoreData;
  goalSequence: {
    firstHalf: MatchGoalSequence;
    secondHalf: MatchGoalSequence;
    fullTime: MatchGoalSequence;
  };
};

export enum PlayerBetStatus {
  Win = "win",
  Lose = "lose",
  Draw = "draw",
  HalfWin = "half-win",
  HalfLose = "half-lose",
}

export type MatchScore = {
  home: number;
  away: number;
};

export enum ScoreTeam {
  None,
  Home,
  Away,
}

export type ScoreData = {
  firstHalf: MatchScore;
  fullTime: MatchScore;
  liveScore: MatchScore;
};

export enum OddsAsianHandicapKind {
  Home = "home",
  Away = "away",
}

export enum OddsOverUnderKind {
  Over = "over",
  Under = "under",
}

export enum Odds1x2Kind {
  Home = "home",
  Away = "away",
  Draw = "draw",
}

export enum OddsOddEvenKind {
  Odd = "odd",
  Even = "even",
}

// 106
export enum BetTypeDoubleChanceIndex {
  HomeDraw,
  HomeAway,
  AwayDraw,
}

export enum OddsDoubleChance {
  HomeDraw = "home_draw",
  HomeAway = "home_away",
  AwayDraw = "away_draw",
}

// 107
export enum BetTypeTotalGoalIndex {
  _0_1,
  _2_3,
  _4AndAbove,
  _4_6,
  _7AndAbove,
}

export enum OddsTotalGoalKind {
  _0_1 = "_0_1",
  _2_3 = "_2_3",
  _4AndAbove = "_4_and_above",
  _4_6 = "_4_6",
  _7AndAbove = "7_above",
}

// 108
export enum BetTypeHalfTimeFullTimeIndex {
  HomeHome,
  HomeDraw,
  HomeAway,
  DrawHome,
  DrawDraw,
  DrawAway,
  AwayHome,
  AwayDraw,
  AwayAway,
}

export enum OddsHalfTimeFullTimeKind {
  HomeHome = "home_home",
  HomeDraw = "home_draw",
  HomeAway = "home_away",
  DrawHome = "draw_home",
  DrawDraw = "draw_draw",
  DrawAway = "draw_away",
  AwayHome = "away_home",
  AwayDraw = "away_draw",
  AwayAway = "away_away",
}

// 109
export enum BetTypeHalfTimeFullTimeOddEvenIndex {
  OddOdd,
  OddEven,
  EvenOdd,
  EvenEven,
}

export enum OddsHalfTimeFullTimeOddEvenKind {
  OddOdd = "odd_odd",
  OddEven = "odd_even",
  EvenOdd = "even_odd",
  EvenEven = "even_even",
}

// 110
export enum BetTypeFirstGoalLastGoalIndex {
  HomeFirst,
  HomeLast,
  AwayFirst,
  AwayLast,
  NoGoal,
}

export enum OddsFirstGoalLastGoalKind {
  HomeFirst = "home_first",
  HomeLast = "home_last",
  AwayFirst = "away_first",
  AwayLast = "away_last",
  NoGoal = "no_goal",
}

// 111
export enum BetTypeCorrectScoreIndex {
  CS0_0,
  CS1_0,
  CS0_1,
  CS2_0,
  CS0_2,
  CS2_1,
  CS1_2,
  CS3_0,
  CS0_3,
  CS3_1,
  CS1_3,
  CS3_2,
  CS2_3,
  CS4_0,
  CS0_4,
  CS4_1,
  CS1_4,
  CS4_2,
  CS2_4,
  CS4_3,
  CS3_4,
  CS1_1,
  CS2_2,
  CS3_3,
  CS4_4,
  AnyOtherScore,
}

export enum OddsCorrectScoreKind {
  CS0_0 = "cs_0_0",
  CS1_0 = "cs_1_0",
  CS0_1 = "cs_0_1",
  CS2_0 = "cs_2_0",
  CS0_2 = "cs_0_2",
  CS2_1 = "cs_2_1",
  CS1_2 = "cs_1_2",
  CS3_0 = "cs_3_0",
  CS0_3 = "cs_0_3",
  CS3_1 = "cs_3_1",
  CS1_3 = "cs_1_3",
  CS3_2 = "cs_3_2",
  CS2_3 = "cs_2_3",
  CS4_0 = "cs_4_0",
  CS0_4 = "cs_0_4",
  CS4_1 = "cs_4_1",
  CS1_4 = "cs_1_4",
  CS4_2 = "cs_4_2",
  CS2_4 = "cs_2_4",
  CS4_3 = "cs_4_3",
  CS3_4 = "cs_3_4",
  CS1_1 = "cs_1_1",
  CS2_2 = "cs_2_2",
  CS3_3 = "cs_3_3",
  CS4_4 = "cs_4_4",
  AnyOtherScore = "any_other_score",
}

// 112
export enum BetTypeBothOneNeitherToScoreIndex {
  Neither,
  One,
  Both,
}

export enum OddsBothOneNeitherToScoreKind {
  Neither = "neither",
  One = "one",
  Both = "both",
}

// 113
export enum BetTypeThreeWayHandicapIndex {
  Handicap,
  FavouriteType,
  Home,
  Away,
  Draw,
}

export enum OddsThreeWayHandicapKind {
  Home = "home",
  Away = "away",
  Draw = "draw",
}

// 114
export enum BetTypeCleanSheetIndex {
  HomeYes,
  HomeNo,
  AwayYes,
  AwayNo,
}

export enum OddsCleanSheetKind {
  HomeYes = "home_yes",
  HomeNo = "home_no",
  AwayYes = "away_yes",
  AwayNo = "away_no",
}

// 104, 115, 116
export enum BetTypeMoneyLineIndex {
  Home,
  Away,
}

export enum OddsMoneyLineKind {
  Home = "home",
  Away = "away",
}

// 102, 133, 134
export enum BetTypeOverUnderIndex {
  Handicap,
  Over,
  Under,
}

// 117
export enum BetTypeWinningMarginIndex {
  HomeLeads1,
  HomeLeads2,
  HomeLeads2Up,
  HomeLeads3Up,
  OtherDraw,
  AwayLeads1,
  AwayLeads2,
  AwayLeads2Up,
  AwayLeads3Up,
  NoGoal,
}

export enum OddsWinningMarginKind {
  HomeLeads1 = "home_leads_1",
  HomeLeads2 = "home_leads_2",
  HomeLeads2Up = "home_leads_2_up",
  HomeLeads3Up = "home_leads_3_up",
  OtherDraw = "other_draw",
  AwayLeads1 = "away_leads_1",
  AwayLeads2 = "away_leads_2",
  AwayLeads2Up = "away_leads_2_up",
  AwayLeads3Up = "away_leads_3_up",
  NoGoal = "no_goal",
}

// 118, 119, 120
export enum BetTypeHighestScoringHalfIndex {
  FirstHalf,
  SecondHalf,
  Tie,
}

export enum OddsHighestScoringHalfKind {
  FirstHalf = "first_half",
  SecondHalf = "second_half",
  Tie = "tie",
}

// 121, 122, 123, 124, 125,
// 126, 127, 128, 132
export enum BetTypeHomeWinBothHalvesIndex {
  Yes,
  No,
}

export enum OddsHomeWinBothHalvesKind {
  Yes = "yes",
  No = "no",
}

export enum OddsAwayWinBothHalvesKind {
  Yes = "yes",
  No = "no",
}

export enum OddsHomeWinEitherHalvesKind {
  Yes = "yes",
  No = "no",
}

export enum OddsAwayWinEitherHalvesKind {
  Yes = "yes",
  No = "no",
}

export enum OddsHomeScoreBothHalvesKind {
  Yes = "yes",
  No = "no",
}

export enum OddsAwayScoreBothHalvesKind {
  Yes = "yes",
  No = "no",
}

export enum OddsBothHalvesOver1_5Kind {
  Yes = "yes",
  No = "no",
}

export enum OddsBothHalvesUnder1_5Kind {
  Yes = "yes",
  No = "no",
}

// 129, 130, 131
export enum BetTypeBothTeamToScoreIn1H2HIndex {
  YesYes,
  YesNo,
  NoYes,
  NoNo,
}

export enum OddsBothTeamToScoreIn1H2HKind {
  YesYes = "yes_yes",
  YesNo = "yes_no",
  NoYes = "no_yes",
  NoNo = "no_no",
}

export enum OddsHomeTeamToScoreIn1H2HKind {
  YesYes = "yes_yes",
  YesNo = "yes_no",
  NoYes = "no_yes",
  NoNo = "no_no",
}

export enum OddsAwayTeamToScoreIn1H2HKind {
  YesYes = "yes_yes",
  YesNo = "yes_no", 
  NoYes = "no_yes",
  NoNo = "no_no",
}

export enum OddsBothTeamToScoreKind {
  Yes = "yes",
  No = "no",
}

export type OddsKind<T> = T extends BetType.FTAH
  ? OddsAsianHandicapKind
  : T extends BetType.FHAH
  ? OddsAsianHandicapKind
  : T extends BetType.FTOU
  ? OddsOverUnderKind
  : T extends BetType.FHOU
  ? OddsOverUnderKind
  : T extends BetType.FT1X2
  ? Odds1x2Kind
  : T extends BetType.FH1X2
  ? Odds1x2Kind
  : T extends SupportedBetType.OddEven
  ? OddsOddEvenKind
  : T extends SupportedBetType.DoubleChance
  ? OddsDoubleChance
  : T extends SupportedBetType.TotalGoal
  ? OddsTotalGoalKind
  : T extends SupportedBetType.HalfTime_FullTime
  ? OddsHalfTimeFullTimeKind
  : T extends SupportedBetType.HalfTime_FullTimeOdd_Even
  ? OddsHalfTimeFullTimeOddEvenKind
  : T extends SupportedBetType.FirstGoalAndLastGoal
  ? OddsFirstGoalLastGoalKind
  : T extends SupportedBetType.CorrectScore
  ? OddsCorrectScoreKind
  : T extends SupportedBetType.BothOneNeithertoScore
  ? OddsBothOneNeitherToScoreKind
  : T extends SupportedBetType.ThreeWayHandicap
  ? OddsThreeWayHandicapKind
  : T extends SupportedBetType.CleanSheet
  ? OddsCleanSheetKind
  : T extends SupportedBetType.MoneyLine
  ? OddsMoneyLineKind
  : T extends SupportedBetType.ToWinToNil
  ? OddsMoneyLineKind
  : T extends SupportedBetType.ToWinFromBehind
  ? OddsMoneyLineKind
  : T extends SupportedBetType.HomeTeamOverUnder
  ? OddsOverUnderKind
  : T extends SupportedBetType.AwayTeamOverUnder
  ? OddsOverUnderKind
  : T extends SupportedBetType.WinningMargin
  ? OddsWinningMarginKind
  : T extends SupportedBetType.HighestScoringHalf
  ? OddsHighestScoringHalfKind
  : T extends SupportedBetType.HighestScoringHalfHome
  ? OddsHighestScoringHalfKind
  : T extends SupportedBetType.HighestScoringHalfAway
  ? OddsHighestScoringHalfKind
  : T extends SupportedBetType.HomeWinBothHalves
  ? OddsHomeWinBothHalvesKind
  : T extends SupportedBetType.AwayWinBothHalves
  ? OddsAwayWinBothHalvesKind
  : T extends SupportedBetType.HomeWinEitherHalves
  ? OddsHomeWinEitherHalvesKind
  : T extends SupportedBetType.AwayWinEitherHalves
  ? OddsAwayWinEitherHalvesKind
  : T extends SupportedBetType.HomeScoreBothHalves
  ? OddsHomeScoreBothHalvesKind
  : T extends SupportedBetType.AwayScoreBothHalves
  ? OddsAwayScoreBothHalvesKind
  : T extends SupportedBetType.BothHalvesOver1_5
  ? OddsBothHalvesOver1_5Kind
  : T extends SupportedBetType.BothHalvesUnder1_5
  ? OddsBothHalvesUnder1_5Kind
  : T extends SupportedBetType.BothTeamtoScoreIn1H_2H
  ? OddsBothTeamToScoreIn1H2HKind
  : T extends SupportedBetType.Home1HtoScore_2HtoScore
  ? OddsHomeTeamToScoreIn1H2HKind
  : T extends SupportedBetType.Away1HtoScore_2HtoScore
  ? OddsAwayTeamToScoreIn1H2HKind
  : T extends SupportedBetType.BothTeamtoScore
  ? OddsBothTeamToScoreKind
  : any;

export type MatchScoreAndHandicap = MatchScore & { handicap: number };
