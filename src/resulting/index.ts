import * as _ from "lodash";
// import { resultingOddEven } from "./105";
import { resultingMoneyLine } from "./by-bet-types/money-line";
import { Bet, SupportedBetType, BetType } from "./types";

import { resultingOddEven } from "./by-bet-types/odd-even";
import { resultingDoubleChance } from "./by-bet-types/double-chance";
import {
  isBetOnMoneyLine,
  isBetOnOddEven,
  isBetOnDoubleChance,
  isBetOnTotalGoal,
  isBetOnHalfTimeFullTime,
  isBetOnHalfTimeFullTimeOddEven,
  isBetOnFirstGoalLastGoal,
  isBetOnCorrectScore,
  isBetOnBothOneNeitherScore,
  isBetOnThreeWayHandicap,
  isBetOnCleansheet,
  isBetOn1x2,
  isBetOnAsianHandicap,
  isBetOnOverUnder,
  isBetOnToWinFromBehind,
  isBetOnWinningMargin,
  isBetOnHighestScoringHalf,
  isBetOnHighestScoringHalfHome,
  isBetOnHighestScoringHalfAway,
  isBetOnHomeWinBothHalves,
  isBetOnAwayWinBothHalves,
  isBetOnHomeWinEitherHalves,
  isBetOnAwayWinHeigherHalves,
  isBetOnBothHalvesOver_1_5,
  isBetOnBothHalvesUnder_1_5,
  isBetOnBothTeamToScoreIn1H2H,
  isBetOnAway1HToScore2HToScore,
  isBetOnBothTeamToScore,
  isBetOnHomeTeamOverUnder,
  isBetOnAwayTeamOverUnder,
  isBetOnToWinToNil,
  isBetOnHomeScoreBothHalves,
  isBetOnAwayScoreBothHalves,
  isBetOnHome1HToScore2HToScore,
} from "./cond/betting-on-type";
import { otherwiseError, ErrorNotSupportedBetTypes } from "./cond/misc";
import { resultingTotalGoal } from "./by-bet-types/total-goal";
import { resultingHalfTimeFullTime } from "./by-bet-types/halftime-fulltime";
import { resultingHalfTimeFullTimeOddEven } from "./by-bet-types/halftime-fulltime-oddeven";
import { resultingFirstGoalLastGoal } from "./by-bet-types/first-goal-last-goal";
import { resultingCorrectScore } from "./by-bet-types/correct-score";
import { resultingBothOneNeitherScore } from "./by-bet-types/both-one-neither-score";
import { resultingThreewayHandicap } from "./by-bet-types/threeway-handicap";
import { resultingCleansheet } from "./by-bet-types/clean-sheet";
import { resulting1x2 } from "./by-bet-types/_1x2";
import { resultingAsianHandicap } from "./by-bet-types/asian-handicap";
import { resultingOverunder } from "./by-bet-types/over-under";
import { resultingWinningMargin } from "./by-bet-types/winning-margin";
import { resultingHighestScoringHalf } from "./by-bet-types/highest-scoring-half";
import { resultingHighestScoringHalfHome } from "./by-bet-types/highest-scoring-half-home";
import { resultingHighestScoringHalfAway } from "./by-bet-types/highest-scoring-half-away";
import { resultingHomeWinBothHalves } from "./by-bet-types/home-win-both-halves";
import { resultingAwayWinBothHalves } from "./by-bet-types/away-win-both-halves";
import { resultingHomeWinEitherHalves } from "./by-bet-types/home-win-either-halves";
import { resultingAwayWinEitherHalves } from "./by-bet-types/away-win-either-halves";
import { resultingHomeScoreBothHalves } from "./by-bet-types/home-score-both-halves";
import { resultingAwayScoreBothHalves } from "./by-bet-types/away-score-both-halves";
import { resultingBothHalvesOver1_5 } from "./by-bet-types/both-halves-over-1.5";
import { resultingBothHalvesUnder1_5 } from "./by-bet-types/both-halves-under-1.5";
import { resultingBothTeamToScoreIn1H2H } from "./by-bet-types/both-team-to-score-in-1h2h";
import { resultingHome1HtoScore_2HtoScore } from "./by-bet-types/home-1h-to-score-2h-to-score";
import { resultingAway1HtoScore_2HtoScore } from "./by-bet-types/away-1h-to-score-2h-to-score";
import { resultingBothTeamToScore } from "./by-bet-types/both-team-to-score";
import { resultingHomeTeamOverUnder } from "./by-bet-types/home-team-over-under";
import { resultingAwayTeamOverUnder } from "./by-bet-types/away-over-under";
import { resultingToWinToNil } from "./by-bet-types/to-win-to-nil";
import { resultingToWinFromBehind } from "./by-bet-types/to-win-from-behind";

export const betResult = _.cond<Bet<SupportedBetType | BetType>, unknown>([
  [isBetOnAsianHandicap, resultingAsianHandicap],
  [isBetOnOverUnder, resultingOverunder],
  [isBetOn1x2, resulting1x2],
  [isBetOnMoneyLine, resultingMoneyLine],
  [isBetOnOddEven, resultingOddEven],
  [isBetOnDoubleChance, resultingDoubleChance],
  [isBetOnTotalGoal, resultingTotalGoal],
  [isBetOnHalfTimeFullTime, resultingHalfTimeFullTime],
  [isBetOnHalfTimeFullTimeOddEven, resultingHalfTimeFullTimeOddEven],
  [isBetOnFirstGoalLastGoal, resultingFirstGoalLastGoal],
  [isBetOnCorrectScore, resultingCorrectScore],
  [isBetOnBothOneNeitherScore, resultingBothOneNeitherScore],
  [isBetOnThreeWayHandicap, resultingThreewayHandicap],
  [isBetOnCleansheet, resultingCleansheet],
  [isBetOnToWinToNil, resultingToWinToNil],
  [isBetOnToWinFromBehind, resultingToWinFromBehind],
  [isBetOnWinningMargin, resultingWinningMargin],
  [isBetOnHighestScoringHalf, resultingHighestScoringHalf],
  [isBetOnHighestScoringHalfHome, resultingHighestScoringHalfHome],
  [isBetOnHighestScoringHalfAway, resultingHighestScoringHalfAway],
  [isBetOnHomeWinBothHalves, resultingHomeWinBothHalves],
  [isBetOnAwayWinBothHalves, resultingAwayWinBothHalves],
  [isBetOnHomeWinEitherHalves, resultingHomeWinEitherHalves],
  [isBetOnAwayWinHeigherHalves, resultingAwayWinEitherHalves],
  [isBetOnHomeScoreBothHalves, resultingHomeScoreBothHalves],
  [isBetOnAwayScoreBothHalves, resultingAwayScoreBothHalves],
  [isBetOnBothHalvesOver_1_5, resultingBothHalvesOver1_5],
  [isBetOnBothHalvesUnder_1_5, resultingBothHalvesUnder1_5],
  [isBetOnBothTeamToScoreIn1H2H, resultingBothTeamToScoreIn1H2H],
  [isBetOnHome1HToScore2HToScore, resultingHome1HtoScore_2HtoScore],
  [isBetOnAway1HToScore2HToScore, resultingAway1HtoScore_2HtoScore],
  [isBetOnBothTeamToScore, resultingBothTeamToScore],
  [isBetOnHomeTeamOverUnder, resultingHomeTeamOverUnder],
  [isBetOnAwayTeamOverUnder, resultingAwayTeamOverUnder],
  otherwiseError(ErrorNotSupportedBetTypes),
]);
