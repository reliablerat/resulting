import {
  Period,
  Bet,
  GoalSequence,
  FavouriteType,
  BetType,
  OddsAsianHandicapKind,
  SupportedBetType,
  OddsMoneyLineKind,
  OddsOddEvenKind,
  OddsDoubleChance,
  OddsTotalGoalKind,
  OddsHalfTimeFullTimeKind,
  OddsHalfTimeFullTimeOddEvenKind,
  OddsFirstGoalLastGoalKind,
  OddsCorrectScoreKind,
  OddsBothOneNeitherToScoreKind,
  OddsCleanSheetKind,
  Odds1x2Kind,
  OddsWinningMarginKind,
  OddsHighestScoringHalfKind,
  OddsHomeWinBothHalvesKind,
  OddsAwayWinBothHalvesKind,
  OddsHomeWinEitherHalvesKind,
  OddsAwayWinEitherHalvesKind,
  OddsHomeScoreBothHalvesKind,
  OddsAwayScoreBothHalvesKind,
  OddsBothHalvesOver1_5Kind,
  OddsBothHalvesUnder1_5Kind,
  OddsHomeTeamToScoreIn1H2HKind,
  OddsAwayTeamToScoreIn1H2HKind,
  OddsBothTeamToScoreIn1H2HKind,
  OddsBothTeamToScoreKind,
  OddsOverUnderKind,
  OddsThreeWayHandicapKind,
} from "./resulting/types";
import { betResult } from "./resulting";
import {
  useActualScore,
  useFirstHalfScore,
  useSubtractHomeAwayScore,
  useFullTimeScore,
} from "./resulting/cond/score";
import {
  flow,
  over,
  cond,
  head,
  tail,
  curry,
  eq,
  last,
  isNull,
  stubTrue,
  constant,
  isArray,
} from "lodash";
import { usePeriod } from "resulting/cond/period";

const bet: Bet<BetType.FHAH> = {
  betCode: BetType.FHAH,
  score: {
    firstHalf: {
      home: 0,
      away: 0,
    },
    fullTime: {
      home: 1,
      away: 3,
    },
    liveScore: {
      home: 0,
      away: 0,
    },
  },
  goalSequence: {
    firstHalf: {
      firstGoal: GoalSequence.NoGoal,
      lastGoal: GoalSequence.NoGoal,
    },
    secondHalf: {
      firstGoal: GoalSequence.HomeFirst,
      lastGoal: GoalSequence.AwayLast,
    },
    fullTime: {
      firstGoal: GoalSequence.HomeFirst,
      lastGoal: GoalSequence.AwayLast,
    },
  },
  favType: FavouriteType.Away,
  betPosition: OddsAsianHandicapKind.Home,
  period: Period.firstHalf,
  handicap: 0.25,
};

console.log({
  // a: y(ccc),
  // betCode: bet.betCode,
  // period: bet.period,
  result: betResult(bet),
  // d: useActualScore(useFullTimeScore)(bet),
  // actualScore: useActualScore(useFirstHalfScore)(bet),
  // c: flow(useActualScore(useFirstHalfScore), useSubtractHomeAwayScore)(bet),
});
// console.log(combineSecondHalfScore(bet));
